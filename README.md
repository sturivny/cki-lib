# cki-lib

Common place to put scripts used across CKI infrastructure.

## Maintainers

* [mh21](https://gitlab.com/mh21)
* [inakimalerba](https://gitlab.com/inakimalerba)

## Design principles

This repository holds common code that is used by at least two other CKI
Project applications.

## Container entrypoint script `cki_entrypoint.sh`

The entrypoint script provides a best-practice container entrypoint script for
CKI Python container images. It is configured via environment variables:

| Variable            | Description                                                                                    |
|---------------------|------------------------------------------------------------------------------------------------|
| `START_REDIS`       | Start a Redis server                                                                           |
| `START_CELERY_xxxx` | Start Celery workers with the given command line arguments, one worker per variable            |
| `START_FLASK`       | Start the given Flask app via gunicorn (`IS_PRODUCTION=true`) or the Flask devel server        |
| `START_PYTHON_xxxx` | Start Python modules with the given command line arguments, one module per variable            |
| `LOG_NAME`          | Append all stdout/stderr output to `/logs/$LOG_NAME-$HOSTNAME.log`                             |
| `LOG_USE_HOSTNAME`  | If false, do not include the host name in the log file name, i.e. log to `/logs/$LOG_NAME.log` |

The following example starts a Redis server, two celery workers and a Flask app:

```shell
#!/bin/bash
export START_REDIS=true
export START_CELERY_MATCHER="--app pipeline_herder.worker --concurrency 1 --queues matching"
export START_CELERY_HERDER="--app pipeline_herder.worker --concurrency 1 --queues herding"
export START_FLASK=pipeline_herder.webhook
export LOG_NAME=pipeline-herder
exec cki_entrypoint.sh
```

**It is important that the entrypoint script is started via exec! Otherwise, the process tree cannot be killed on failures!**

## `gitlab-yaml-shellcheck`

Runs `shellcheck` on the shell code in the different jobs for a GitLab CI/CD
pipeline `gitlab-ci.yml`.

Usage:

```shell
gitlab-yaml-shellcheck [--job JOB] [options] pipeline-file
```

By default, all jobs are checked. The shell scripts for `before_script`,
`script` and `after_script` are extracted and combined with the corresponding
variables into a temporary shell script, which is then checked by `shellcheck`.
When `--checked-sourced` is specified, messages for sourced scripts are shown
as well. Any unknown arguments are passed to shellcheck.

Without `--checked-sourced`, an offline cache with `shellcheck` output in
`~/.cache/gitlab-yaml-shellcheck` is used to try to avoid checking unmodified
jobs. The cache key depends on the script contents, line numbers and shellcheck
options. The cache will not be invalidated if any of the sourced scripts
change. If this is required, use `--no-cache`.

For [ALE/Vim](https://github.com/dense-analysis/ale), an ALE linter definition
for pipeline YAMLs is provided in `vim/ale_linters`. To use it, add the `vim`
directory to your VIM runtime path via

```text
set runtimepath+=/path/to/cki-lib/vim
```

## `cki_lint.sh`

Runs `flake8`, `pydocstyle`, `isort --check` and `pytest` under `coverage`. The
script takes care to install the needed dependencies via pip beforehand. For
`pylint` and `pytest`, only the packages specified as parameters will be checked.

The experimental `--fix` parameter can be used to automatically fix some of the
detected issues. At the moment, this will invoke `autopep8` and `isort` without
`--check`.

For correct editor integration of linters/fixers, the following parameters must
be set:

- `flake8`: `--max-line-length 100`
- `autopep8`: `--max-line-length 100`
- `isort`: `--line-length 100 --force-single-line-imports --force-sort-within-sections`

When run in a GitLab CI/CD pipeline for a merge request, the script tries to
determine the previous code coverage and will fail if code coverage decreased.
This can be skipped by adding `[skip coverage check]` with an explanation to
the commit message.

## Developer guidelines

**Use care when making changes to this repository.** Many other applications
depend on this code to work properly.

### Setup

If you want to get started with development, you can install the
dependencies by running:

```shell
pip install --user .[dev]
sudo dnf install shellcheck
```

### Tests + linting

To run the tests (together with linting), execute the following command:

```shell
tox
```
