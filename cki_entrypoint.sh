#!/bin/bash

set -euo pipefail

function main() {
    echo "Started: $(date -Iseconds)"

    echo "PID shell: $$"

    if [ -v START_REDIS ]; then
        /usr/bin/redis-server /etc/redis/redis.conf &
        echo "PID redis: $!"
    fi

    # this needs to happen before other Python modules are started
    # so prometheus_multiproc_dir is exposed to them
    if [ -v START_STANDALONE_METRICS_SERVER ]; then
        export prometheus_multiproc_dir=/tmp/metrics
        mkdir -p "${prometheus_multiproc_dir}"
        # shellcheck disable=SC2154
        gunicorn --bind "0.0.0.0:${CKI_METRICS_PORT}" --workers 1 "cki_lib.metrics.server:run" &
        echo "PID metrics server: $!"
    fi

    for CELERY_VAR in "${!START_CELERY_@}"; do
        IFS=' ' read -r -a ARGS <<< "${!CELERY_VAR}"
        celery worker "${ARGS[@]}" &
        echo "PID ${CELERY_VAR}: $!"
    done

    if [ -v START_FLASK ]; then
        if [[ "${IS_PRODUCTION:-}" = [Tt]rue ]] ; then
            gunicorn --bind 0.0.0.0:5000 --workers "${WEB_WORKERS:-2}" "${START_FLASK}:${FLASK_APP:-app}" &
        else
            FLASK_APP="${START_FLASK}:${FLASK_APP:-app}" FLASK_ENV=development flask run --host 0.0.0.0 --port 5000 &
        fi
        echo "PID flask: $!"
    fi

    # deprecated, can be removed once all users have switched to START_PYTHON_xxx syntax
    if [ -v START_PYTHON ]; then
        IFS=' ' read -r -a ARGS <<< "${START_PYTHON}"
        python3 -m "${ARGS[@]}" &
        echo "PID python module: $!"
    fi

    # shellcheck disable=SC2153
    for PYTHON_VAR in "${!START_PYTHON_@}"; do
        IFS=' ' read -r -a ARGS <<< "${!PYTHON_VAR}"
        python3 -m "${ARGS[@]}" &
        echo "PID ${PYTHON_VAR}: $!"
    done

    # If even one process exits, kill all spawned processes
    echo "Waiting for first process to exit"
    wait -n || true

    echo "Killing process group: -$$"
    kill -- -$$
}

# make getpwuid_r happy
if [ -w '/etc/passwd' ] && ! id -nu > /dev/null 2>&1; then
    echo "cki:x:$(id -u):$(id -g):,,,:${HOME}:/bin/bash" >> /etc/passwd;
fi

# initialize kerberos keytab
if [ -r "${KRB_KEYTAB:-/keytab}" ]; then
    # shellcheck disable=SC2154
    kinit -t "${KRB_KEYTAB:-/keytab}" "${KRB_USER}" -l 7d
fi

if [ -v LOG_NAME ]; then
    if [[ "${LOG_USE_HOSTNAME:-True}" = [Tt]rue ]]; then
        LOG_PATH="/logs/${LOG_NAME}-${HOSTNAME}.log"
    else
        LOG_PATH="/logs/${LOG_NAME}.log"
    fi
    main 2>&1 | tee -a "${LOG_PATH}"
else
    main
fi &

# For ^C and SIGTERM for the container, kill the whole process group
trap 'trap - SIGINT SIGTERM && kill -- -$$' SIGINT SIGTERM
wait -n
