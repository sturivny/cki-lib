"""Gitlab helper."""
import os
import re
from urllib import parse

import gitlab
import yaml

from cki_lib.logger import get_logger
from cki_lib.session import get_session

GITLAB_TIMESTAMP_FORMAT = "%Y-%m-%dT%H:%M:%S.%fZ"

LOGGER = get_logger(__name__)
SESSION = get_session(__name__)


def get_token(url, env_name=None):
    """Return a Gitlab API token.

    The tokens have to be provided as environment variables like this:
        export GITLAB_TOKENS='{"gitlab.com": "COM_GITLAB_TOKEN"}'
        export COM_GITLAB_TOKEN='1234567890abcedf'

    Parameters:
        url: GitLab instance URL
        env_name: name of the environment variable with a JSON host=name dict
            containing the names of the environment variables with the tokens
    """
    host = parse.urlsplit(url).hostname
    # would be nice to get tokens from a '.config/cki/config'/CKI_CONFIG file
    token_names = yaml.safe_load(
        os.environ.get(env_name or 'GITLAB_TOKENS', '{}'))
    token_name = token_names.get(host)
    return os.environ.get(token_name) if token_name else None


def get_instance(url, token=None, env_name=None):
    """Return a Gitlab API instance.

    Parameters:
        url: GitLab instance URL
        token: private GitLab API token
        env_name: environment variable name to use for get_token()
    """
    return gitlab.Gitlab(url, session=SESSION,
                         private_token=token or get_token(url, env_name))


def get_variables(gl_pipeline):
    """Return a dict with the pipeline variables."""
    return {v.key: v.value
            for v in gl_pipeline.variables.list(as_list=False)}


def parse_gitlab_url(url):
    """Parse a GitLab URL and return a Gitlab object.

    At the moment, the following URL formats are supported:

    Projects:
        https://gitlab.com/group/project
    Pipelines:
        https://gitlab.com/group/project/-/pipelines/1234
    MRs:
        https://gitlab.com/group/project/-/merge_requests/1234
    MR notes:
        https://gitlab.com/group/project/-/merge_requests/1234#note_1234567890
    Pipeline schedule:
        https://gitlab.com/group/project/-/pipeline_schedules/1234

    Args:
        url: Full GitLab URL.

    Returns:
        (gitlab.Gitlab, Object for the URL)
    """
    url_parts = parse.urlsplit(url)

    instance_url = parse.urlunparse(url_parts[:2] + ('',) * 4)
    instance = get_instance(instance_url)

    # Match with pipeline schedule URL
    if '/-/pipeline_schedules' in url_parts.path:
        match = re.match(r'/(.*)/-/pipeline_schedules/(\d+)', url_parts.path)
        project = instance.projects.get(match[1], lazy=True)
        schedule = project.pipelineschedules.get(int(match[2]))
        return instance, schedule

    # Match with Pipeline URL
    if '/-/pipelines' in url_parts.path:
        match = re.match(r'/(.*)/-/pipelines/(\d+)', url_parts.path)
        project = instance.projects.get(match[1], lazy=True)
        pipeline = project.pipelines.get(int(match[2]))
        return instance, pipeline

    # Match with Merge Request URL
    if '/-/merge_requests' in url_parts.path:
        match = re.match(r'/(.*)/-/merge_requests/(\d+)', url_parts.path)
        project = instance.projects.get(match[1], lazy=True)
        mergerequest = project.mergerequests.get(int(match[2]))

        # MR Notes are on the same URL as the Merge Request
        if url_parts.fragment and 'note_' in url_parts.fragment:
            note_id = url_parts.fragment.replace('note_', '')
            note = mergerequest.notes.get(note_id)
            return instance, note

        return instance, mergerequest

    # Match with Issue URL
    if '/-/issues' in url_parts.path:
        match = re.match(r'/(.*)/-/issues/(\d+)', url_parts.path)
        project = instance.projects.get(match[1], lazy=True)
        issue = project.issues.get(int(match[2]))
        return instance, issue

    # Fallback to project
    project = instance.projects.get(url_parts.path[1:])
    return instance, project
