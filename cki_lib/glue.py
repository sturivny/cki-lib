"""Reusable glue logic."""
import re


def do_xml_replacements(line_lst, replacements):
    """Generate job XML with template replacements applied.

    Search the template for words surrounded by "##" strings and replace them
    with strings from the supplied dictionary.

    Args:
        line_lst:       An iterable that contains lines of XML file
        replacements:   A dictionary of placeholder strings with "##"
                        around them, and their replacements.

    Raises:
        ValueError if the placeholder would be replaced by a non-string
                   object.

    Returns:
        The job XML text with template replacements applied.
    """
    xml = []
    for line in line_lst:
        for match in re.finditer(r"##(\w+)##", line):
            to_replace = match.group(1)
            if to_replace in replacements:
                if not isinstance(replacements[to_replace], str):
                    raise ValueError('XML replace: string expected but'
                                     f' {to_replace} is {replacements[to_replace]}')
                line = line.replace(match.group(0),
                                    replacements[to_replace])
        xml.append(line)

    return '\n'.join(xml)
