"""Common CKI logging functions."""
import contextlib
import datetime
import glob
import json
import logging
import os
import subprocess
import sys
import threading
import typing

CKI_LOGS_FILEDIR = '/tmp/cki-lib.log/'
STREAM = sys.stderr
FORMAT = '%(asctime)s.%(msecs)03d - [%(levelname)s] - %(name)s - %(message)s'
FORMAT_DATE = '%Y-%m-%dT%H:%M:%S'
LOCK = threading.Lock()
CKI_HANDLER = False

LOGGING_ENV = threading.local()


@contextlib.contextmanager
def logging_env(variables: typing.Dict[str, typing.Any]) -> typing.Iterator[None]:
    """Set logging environment variables."""
    LOGGING_ENV.__dict__.update(variables)
    try:
        yield
    finally:
        for variable in variables:
            del LOGGING_ENV.__dict__[variable]


class JSONFormatter(logging.Formatter):
    """JSON logging formatter."""

    def format(self, record: logging.LogRecord) -> str:
        """Format record into json."""
        content = {
            'timestamp': record.created,
            'logger': {
                'name': record.name,
                'level': record.levelname,
            },
            'message': record.msg,
            'extras': LOGGING_ENV.__dict__,
        }
        record.msg = json.dumps(content, default=self.default)
        return super().format(record)

    @staticmethod
    def default(obj: typing.Any) -> str:
        """Safely try to dump any object."""
        if isinstance(obj, datetime.datetime):
            return obj.isoformat()
        try:
            return str(obj)
        except Exception:  # pylint: disable=broad-except
            return f'invalid: {obj.__class__.__name__}'


class PackFormatter(logging.Formatter):
    """JSON logging formatter for Loki '| unpack'."""

    def format(self, record: logging.LogRecord) -> str:
        """Format record into json."""
        content = {
            'timestamp': str(record.created),
            'logger_name': record.name,
            'logger_level': record.levelname,
            '_entry': record.msg,
        }
        self.flatten(content, 'extras', LOGGING_ENV.__dict__)
        record.msg = json.dumps(content)
        return super().format(record)

    @classmethod
    def flatten(
        cls,
        output: typing.Dict[str, str],
        prefix: str,
        obj: typing.Dict[str, typing.Any],
    ) -> None:
        """Safely try to dump any object."""
        try:
            if isinstance(obj, dict):
                for key, value in obj.items():
                    cls.flatten(output, f'{prefix}_{key}', value)
            elif isinstance(obj, list):
                for key, value in enumerate(obj):
                    cls.flatten(output, f'{prefix}_{key}', value)
            elif isinstance(obj, datetime.datetime):
                output[prefix] = obj.isoformat()
            else:
                output[prefix] = str(obj)
        except Exception:  # pylint: disable=broad-except
            output[prefix] = f'invalid: {obj.__class__.__name__}'


def get_logger(logger_name: str) -> logging.Logger:
    """Return CKI logger or descendant."""
    # Make sure adding handler is thread safe since get_logger might not be
    # called from main
    # https://docs.python.org/3/library/logging.html#logging.basicConfig
    with LOCK:
        global CKI_HANDLER  # pylint: disable=global-statement
        cki_handler = CKI_HANDLER
        CKI_HANDLER = True

    if not cki_handler:
        # Add STREAM handler to root logger.
        root_logger = logging.getLogger()

        handler = logging.StreamHandler(STREAM)
        root_logger.addHandler(handler)
        logging_format = os.environ.get('CKI_LOGGING_FORMAT', 'plain')
        if logging_format == 'json':
            formatter: logging.Formatter = JSONFormatter()
        elif logging_format == 'pack':
            formatter = PackFormatter()
        else:
            formatter = logging.Formatter(fmt=FORMAT, datefmt=FORMAT_DATE)
        handler.setFormatter(formatter)

        # Set cki loglevel to CKI_LOGGING_LEVEL.
        cki_logger = logging.getLogger('cki')
        cki_logger.setLevel(os.environ.get('CKI_LOGGING_LEVEL', 'WARNING'))

    if not (logger_name == 'cki' or logger_name.startswith('cki.')):
        logger_name = 'cki.' + logger_name

    return logging.getLogger(logger_name)


def truncate_logs() -> None:
    """Truncate all .log files in the CKI_LOGS_FILEDIR directory."""
    files = glob.glob(f'{CKI_LOGS_FILEDIR}/*.log')
    # empty all logs
    subprocess.run(['truncate', '-s', '0', ] + files, check=True)


def logger_add_fhandler(logger: logging.Logger,
                        dst_file: str,
                        path: typing.Optional[str] = None) -> None:
    """Add filehandler to existing logger."""
    dirname = CKI_LOGS_FILEDIR if path is None else path
    os.makedirs(dirname, exist_ok=True)
    dst_file = os.path.join(dirname, dst_file)

    fhandler = logging.FileHandler(dst_file)
    fhandler.setLevel(logging.DEBUG)
    logger.addHandler(fhandler)


def file_logger(
        logger_name: str,
        dst_file: str = 'info.log',
        stream: typing.TextIO = STREAM,
        stream_level: str = os.environ.get('CKI_LOGGING_LEVEL', 'WARNING'),
) -> logging.Logger:
    """Get CKI logger and add stream and file handlers to it.

    Output goes to dst_file in CKI_LOGS_FILEDIR
    logger_name is the name of the logger.

    Attributes:
        logger_name: str, something to identify distinct loggers
        dst_file: str, default is 'info.log', a filename to use for output
        stream_level: console output loglevel
        stream: None, sys.stderr or sys.stdout

    """
    logger = get_logger(logger_name)

    # Would probably be better to let get_logger handle propagation and
    # logging level, but can't both log DEBUG to file handler and defer to
    # root logger
    logger.propagate = False
    logger.setLevel(logging.DEBUG)

    handler = logging.StreamHandler(stream)
    logger.addHandler(handler)
    formatter = logging.Formatter(FORMAT)
    handler.setFormatter(formatter)
    handler.setLevel(stream_level)

    logger_add_fhandler(logger, dst_file)

    return logger
