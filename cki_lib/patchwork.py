"""Patchwork api interaction."""
import datetime
import re

from furl import furl
import requests


class MaxIterationsReached(Exception):
    """MaxIterationsReached Exception."""


class Patchwork:
    """Helper to talk to the patchwork v2 api."""

    def __init__(self, url, max_pages=100):
        """
        Initialize.

        url:    url of the patchwork instance api. Usually like:
                    http(s)://$domain/api
        """
        self.url = url
        self.max_pages = max_pages

    def _get_paginated(self, url):
        paginated_url = furl(url)
        result = []

        for page_counter in range(1, self.max_pages+1):
            paginated_url.args['page'] = page_counter
            res = requests.get(paginated_url.url)

            if res.status_code == 404:
                break  # Hit the end

            result.append(res.json())

        else:
            raise MaxIterationsReached(
                "Results missing when max_page counter reached."
            )

        return result

    def get_patch(self, patch_id):
        """Get detailed info of a patch."""
        url = f'{self.url}/patches/{patch_id}'
        res = requests.get(url)
        return res.json()

    def get_patch_from_url(self, patch_url):
        """Get patch from a patch url."""
        patch_id = re.findall(r'\/patch\/(\d+)', patch_url)[0]
        return self.get_patch(patch_id)

    def get_patches_since(self, project, since_days=1):
        """
        Get a list of patches submitted since (today - since_days).

        Handles pagination.

        project:    project id or name
        """
        since = datetime.datetime.now(datetime.timezone.utc) -\
            datetime.timedelta(days=since_days)

        url = f'{self.url}/patches?project={project}&since={since.isoformat()}'
        patches = []

        for page in self._get_paginated(url):
            patches.extend(page)

        return patches

    def get_series(self, series_id):
        """Query the list of patches corresponding to a series."""
        url = f'{self.url}/series/{series_id}'
        res = requests.get(url)
        return res.json()

    def get_series_since(self, project, since_days=1):
        """
        Get a list of series submitted since (today - since_days).

        Handles pagination.

        project:    project id or name
        """
        since = datetime.datetime.now() - datetime.timedelta(days=since_days)

        url = f'{self.url}/series?project={project}&since={since.isoformat()}'
        series = []

        for page in self._get_paginated(url):
            series.extend(page)

        return series
