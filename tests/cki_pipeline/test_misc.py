"""Tests for various methods of cki_pipeline."""
# pylint: disable=protected-access
import unittest
from unittest.mock import patch

import responses
import yaml

from cki_lib import cki_pipeline
from cki_lib.gitlab import get_instance

from . import mocks


class TestMisc(unittest.TestCase, mocks.GitLabMocks):
    """Test cases for various functions in the cki_pipeline module."""

    gitlab_ci_yml = {
        'include': [
            {
                'project': 'cki-project/pipeline-definition',
                'file': 'cki_pipeline.yml'
            },
        ]
    }

    def _setup_last_pipeline(self, gitlab_yml=gitlab_ci_yml):
        self.add_project(1, 'cki-project')
        self.add_branch(1, 'branch')
        self.add_gitlab_yml(1, gitlab_yml)
        responses.add(
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines?'
            'ref=branch&scope=finished',
            json=[{'id': 103}, {'id': 102}, {'id': 101}, {'id': 100}, {'id': 99}])
        responses.add(
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines?'
            'ref=branch&scope=finished&status=success',
            json=[{'id': 103}, {'id': 102}, {'id': 100}, {'id': 99}])
        responses.add(  # cki_pipeline_type missing
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines/99/variables',
            json=[])
        responses.add(
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines/100/variables',
            json=[{'key': 'cki_pipeline_type', 'value': 'baseline'}])
        responses.add(  # not successful above
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines/101/variables',
            json=[{'key': 'cki_pipeline_type', 'value': 'baseline'}])
        responses.add(  # wrong cki_pipeline_type
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines/102/variables',
            json=[{'key': 'cki_pipeline_type', 'value': 'other'}])
        responses.add(  # retriggered
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines/103/variables',
            json=[{'key': 'cki_pipeline_type', 'value': 'baseline'},
                  {'key': 'retrigger', 'value': 'true'}])

        return get_instance('https://gitlab.com').projects.get('cki-project')

    @responses.activate
    def test_last_pipeline(self):
        """Check the last pipeline without filter."""
        project = self._setup_last_pipeline()
        self.assertEqual(cki_pipeline.last_pipeline_for_branch(
            project, 'branch',
            list_filter={"scope": 'finished'}
        ).id, 102)

    @responses.activate
    def test_last_pipeline_filter(self):
        """Check the last pipeline with filter."""
        project = self._setup_last_pipeline()
        self.assertEqual(cki_pipeline.last_pipeline_for_branch(
            project, 'branch',
            variable_filter={'cki_pipeline_type': 'baseline'},
            list_filter={"scope": 'finished'}
        ).id, 101)

    @responses.activate
    def test_last_successful_pipeline(self):
        """Check the last successful pipeline without filter."""
        project = self._setup_last_pipeline()
        self.assertEqual(cki_pipeline.last_successful_pipeline_for_branch(
            project, 'branch',
        ).id, 102)

    @responses.activate
    def test_last_successful_pipeline_filter(self):
        """Check the last successful pipeline with filter."""
        project = self._setup_last_pipeline()
        self.assertEqual(cki_pipeline.last_successful_pipeline_for_branch(
            project, 'branch',
            variable_filter={'cki_pipeline_type': 'baseline'},
        ).id, 100)

    @responses.activate
    def test_last_successful_pipeline_missing(self):
        """Check a missing last successful pipeline."""
        project = self._setup_last_pipeline()
        self.assertEqual(cki_pipeline.last_successful_pipeline_for_branch(
            project, 'branch',
            variable_filter={'cki_pipeline_type': 'non-existing'},
        ), None)

    @responses.activate
    def test_last_successful_pipeline_none(self):
        """Check a missing last successful pipeline."""
        project = self._setup_last_pipeline()
        self.assertEqual(cki_pipeline.last_successful_pipeline_for_branch(
            project, 'branch',
            variable_filter={'cki_pipeline_type': None},
        ).id, 99)

    def test_clean_project_url(self):
        """Verify the conversion of repository URLs into GitLab project URLs."""

        self.assertEqual(cki_pipeline._clean_project_url("https://foo.bar/test.git/"),
                         "https://foo.bar/test")
        self.assertEqual(cki_pipeline._clean_project_url("https://foo.bar/test.git"),
                         "https://foo.bar/test")
        self.assertEqual(cki_pipeline._clean_project_url("https://foo.bar/test"),
                         "https://foo.bar/test")
        self.assertEqual(cki_pipeline._clean_project_url("https://foo.bar/test/"),
                         "https://foo.bar/test")
        self.assertEqual(cki_pipeline._clean_project_url("foo.bar/test.git/"),
                         "https://foo.bar/test")
        self.assertEqual(cki_pipeline._clean_project_url("git+https://foo.bar/test.git/"),
                         "https://foo.bar/test")

    @responses.activate
    def test_create_custom_configuration(self):
        """Test for when .gitlab-ci.yml contains url includes"""
        variables_common = {'cki_pipeline_branch': 'cki_pipeline_branch',
                            'title': 'title',
                            'pipeline_definition_branch_override': 'pipeline-branch',
                            }
        variables_git = {**variables_common,
                         'pipeline_definition_repository_override':
                             'https://gitlab.com/pipeline-project/pipeline-definition.git',
                         }
        variables_no_git = {**variables_common,
                            'pipeline_definition_repository_override':
                                'https://gitlab.com/pipeline-project/pipeline-definition',
                            }

        gitlab_ci_yml_urls = {
            'include': [
                'https://gitlab.com/cki-project/pipeline-definition/raw/main/cki_pipeline.yml',
            ]
        }
        expected_yml_project = yaml.dump({
            'include': [
                {
                    'project': 'pipeline-project/pipeline-definition',
                    'file': 'cki_pipeline.yml',
                    'ref': variables_common['pipeline_definition_branch_override']
                },
            ]
        })
        expected_yml_urls = yaml.dump({
            'include': [
                f'{variables_no_git["pipeline_definition_repository_override"]}'
                f'/raw/{variables_common["pipeline_definition_branch_override"]}/cki_pipeline.yml',
            ]
        })

        for gitlab_ci_yml_i, expected_yml in ((self.gitlab_ci_yml, expected_yml_project),
                                              (gitlab_ci_yml_urls, expected_yml_urls)):
            for variables in (variables_git, variables_no_git):
                with self.subTest(gitlab_ci_yml_i=gitlab_ci_yml_i,
                                  expected_yml=expected_yml,
                                  variables=variables):
                    responses.reset()
                    project = self._setup_last_pipeline(gitlab_ci_yml_i)
                    start_sha = 'sha'
                    with patch.object(project.commits, "create") as mock_create:
                        branch = cki_pipeline._create_custom_configuration(project, start_sha,
                                                                           variables)
                        mock_create.assert_called_with({
                            'branch': branch,
                            'start_sha': start_sha,
                            'commit_message': cki_pipeline._commit_message(variables),
                            'actions': [{
                                'action': 'update',
                                'file_path': '.gitlab-ci.yml',
                                'content': expected_yml
                            }]
                        })
