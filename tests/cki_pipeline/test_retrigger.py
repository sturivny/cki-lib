"""Unit tests for cki_pipeline.retrigger()"""
import unittest
from unittest import mock

import responses

from cki_lib import cki_pipeline
from cki_lib.gitlab import get_instance

from . import mocks


class TestRetrigger(unittest.TestCase, mocks.GitLabMocks):
    """Tests for cki_pipeline.retrigger()."""

    variables = {'cki_project': 'cki-project',
                 'cki_pipeline_branch': 'test_branch',
                 'cki_pipeline_type': 'baseline',
                 'title': 'original-title'}

    def _check_variables(self, is_production=True, empty=False, trigger_token=False):
        trigger_requests = self.get_requests(
            url='https://gitlab.com/api/v4/projects/1/trigger/pipeline')
        api_requests = self.get_requests(url='https://gitlab.com/api/v4/projects/1/pipeline')
        if empty:
            self.assertFalse(trigger_requests or api_requests)
            return None
        if trigger_token:
            request_variables = trigger_requests[0]['variables']
        else:
            request_variables = {v['key']: v['value'] for v in api_requests[0]['variables']}
        if is_production:
            self.assertNotIn('retrigger', request_variables)
        else:
            self.assertIn('retrigger', request_variables)
        return request_variables

    @responses.activate
    @mock.patch('cki_lib.cki_pipeline._create_custom_configuration',
                mock.Mock(return_value='branch'))
    @mock.patch('cki_lib.cki_pipeline._create_commit', mock.Mock())
    def _check_migrations(self, variables):
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])
        self.add_branch(1, 'branch')
        self.add_gitlab_yml(1)
        gl_project = gl_instance.projects.get(1)
        self.add_pipeline(1, 1, {
            **self.variables,
            **variables,
        })
        cki_pipeline.retrigger(gl_project, 1)
        return self._check_variables(is_production=False)

    def test_variable_migration_architectures_not_present(self):
        """Check migration for the architectures trigger variable."""
        variables = self._check_migrations({
        })
        self.assertNotIn('architectures', variables)

    def test_variable_migration_architectures(self):
        """Check migration for the architectures trigger variable."""
        variables = self._check_migrations({
            'arch_override': 'arch_override',
        })
        self.assertEqual(variables['architectures'], 'arch_override')

    def test_variable_migration_architectures_present(self):
        """Check migration for the architectures trigger variable."""
        variables = self._check_migrations({
            'arch_override': 'arch_override',
            'architectures': 'architectures',
        })
        self.assertEqual(variables['architectures'], 'architectures')

    def test_variable_migration_kpet_tree_family_not_present(self):
        """Check migration for the kpet_tree_family trigger variable."""
        variables = self._check_migrations({
        })
        self.assertNotIn('kpet_tree_family', variables)

    def test_variable_migration_kpet_tree_family(self):
        """Check migration for the kpet_tree_family trigger variable."""
        variables = self._check_migrations({
            'tree_name': 'tree_name',
        })
        self.assertEqual(variables['kpet_tree_family'], 'tree_name')

    def test_variable_migration_kpet_tree_family_present(self):
        """Check migration for the kpet_tree_family trigger variable."""
        variables = self._check_migrations({
            'tree_name': 'tree_name',
            'kpet_tree_family': 'kpet_tree_family',
        })
        self.assertEqual(variables['kpet_tree_family'], 'kpet_tree_family')

    def test_variable_migration_scratch_not_present(self):
        """Check migration for the scratch trigger variable."""
        variables = self._check_migrations({
        })
        self.assertNotIn('scratch', variables)

    def test_variable_migration_scratch(self):
        """Check migration for the scratch trigger variable."""
        variables = self._check_migrations({
            'is_scratch': 'is_scratch',
        })
        self.assertEqual(variables['scratch'], 'is_scratch')

    def test_variable_migration_scratch_present(self):
        """Check migration for the scratch trigger variable."""
        variables = self._check_migrations({
            'is_scratch': 'is_scratch',
            'scratch': 'scratch',
        })
        self.assertEqual(variables['scratch'], 'scratch')
