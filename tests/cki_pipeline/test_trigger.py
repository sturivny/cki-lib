"""Unit tests for cki_pipeline.trigger()"""
import unittest
from unittest import mock

import responses

from cki_lib import cki_pipeline
from cki_lib.gitlab import get_instance

from . import mocks


class TestTrigger(unittest.TestCase, mocks.GitLabMocks):
    """Tests for cki_pipeline.trigger()."""

    variables = {'cki_project': 'cki-project',
                 'cki_pipeline_branch': 'test_branch',
                 'cki_pipeline_type': 'baseline',
                 'title': 'original-title'}

    def _check_variables(self, is_production=True, empty=False, trigger_token=False):
        trigger_requests = self.get_requests(
            url='https://gitlab.com/api/v4/projects/1/trigger/pipeline')
        api_requests = self.get_requests(url='https://gitlab.com/api/v4/projects/1/pipeline')
        if empty:
            self.assertFalse(trigger_requests or api_requests)
            return None
        if trigger_token:
            request_variables = trigger_requests[0]['variables']
        else:
            request_variables = {v['key']: v['value'] for v in api_requests[0]['variables']}
        if is_production:
            self.assertNotIn('retrigger', request_variables)
        else:
            self.assertIn('retrigger', request_variables)
        return request_variables

    @responses.activate
    @mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
    def test_trigger_token(self):
        """Check handling of a trigger token."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, self.variables, is_production=True,
                             trigger_token='token')
        self._check_variables(trigger_token=True)

    @responses.activate
    @mock.patch('cki_lib.cki_pipeline.sleep')
    def test_is_production_true(self, sleep_mock):
        """Check handling of explicit is_production=True."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, self.variables, is_production=True)
        self._check_variables(is_production=True)
        sleep_mock.assert_not_called()

    @responses.activate
    @mock.patch('cki_lib.cki_pipeline.sleep')
    def test_is_production_false(self, sleep_mock):
        """Check handling of explicit is_production=False."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, self.variables, is_production=False)
        self._check_variables(is_production=False)
        sleep_mock.assert_called_with(30)

    @responses.activate
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('cki_lib.cki_pipeline.sleep')
    def test_is_production_none_true(self, sleep_mock):
        """Check handling of implicit is_production=True."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, self.variables, is_production=None)
        self._check_variables(is_production=True)
        sleep_mock.assert_not_called()

    @responses.activate
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False))
    @mock.patch('cki_lib.cki_pipeline.sleep')
    def test_is_production_none_false(self, sleep_mock):
        """Check handling of implicit is_production=False."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, self.variables, is_production=None)
        self._check_variables(is_production=False)
        sleep_mock.assert_called_with(30)

    @responses.activate
    @mock.patch('builtins.input')
    @mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
    def test_interactive_false(self, input_mock):
        """Check handling of interactive=False."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, self.variables, is_production=True, interactive=False)
        self._check_variables()
        input_mock.assert_not_called()

    @responses.activate
    @mock.patch('builtins.input')
    @mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
    def test_interactive_true_non_prod(self, input_mock):
        """Check handling of interactive=True."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, self.variables, is_production=False, interactive=True)
        self._check_variables(is_production=False)
        input_mock.assert_not_called()

    @responses.activate
    @mock.patch('builtins.input')
    @mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
    def test_interactive_true(self, input_mock):
        """Check handling of interactive=True."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        input_mock.return_value = 'YES'
        cki_pipeline.trigger(gl_project, self.variables, is_production=True, interactive=True)
        self._check_variables()
        input_mock.assert_called()

    @responses.activate
    @mock.patch('builtins.input')
    @mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
    def test_interactive_true_wrong_input(self, input_mock):
        """Check handling of interactive=True."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        input_mock.return_value = 'not-yes'
        self.assertRaises(Exception, lambda: cki_pipeline.trigger(
            gl_project, self.variables, is_production=True, interactive=True))
        self._check_variables(empty=True)
        input_mock.assert_called()

    @responses.activate
    @mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
    def test_variable_cleaning(self):
        """Check cleaning of variables for is_production=False."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, dict(self.variables, **{
            'send_report_to_upstream': 'send_report_to_upstream',
            'skip_rhcheckpatch': 'skip_rhcheckpatch',
            'mail_to': 'mail_to',
            'mail_cc': 'mail_cc',
            'mail_bcc': 'mail_bcc',
            'mail_add_maintainers_to': 'mail_add_maintainers_to',
            'ystream_distro': 'ystream_distro',
            'ystream_buildroot': 'ystream_buildroot',
            'test_priority': 'urgent',
            'skip_beaker': 'False',
            'report_rules': '[{"if": "cond", "send_cc": "recipient"}]'
        }), is_production=False)
        variables = self._check_variables(is_production=False)
        self.assertEqual(variables, {
            'cki_project': 'cki-project',
            'cki_pipeline_branch': 'test_branch',
            'cki_pipeline_type': 'retrigger',
            'title': 'Retrigger: original-title',
            'subject': 'Retrigger: original-title',
            'retrigger': 'true',
            'send_report_to_upstream': 'false',
            'skip_rhcheckpatch': 'true',
            # 'mail_to': removed
            # 'mail_cc': removed
            # 'mail_bcc': removed
            # 'mail_add_maintainers_to': removed
            # 'ystream_distro': removed
            # 'ystream_buildroot': removed
            # 'report_rules': removed
            # 'test_priority': removed
            'skip_beaker': 'true'})

    @responses.activate
    @mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
    def test_variable_cleaning_force(self):
        """Check cleaning of forced variables for is_production=False."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, dict(self.variables, **{
            # 'send_report_to_upstream': missing
            # 'skip_rhcheckpatch': missing
            # 'mail_to': missing
            # 'mail_cc': missing
            # 'mail_bcc': missing
            # 'mail_add_maintainers_to': missing
            # 'ystream_distro': missing
            # 'ystream_buildroot': missing
            # 'test_priority': missing
            # 'skip_beaker': missing
        }), is_production=False)
        variables = self._check_variables(is_production=False)
        self.assertEqual(variables, {
            'cki_project': 'cki-project',
            'cki_pipeline_branch': 'test_branch',
            'cki_pipeline_type': 'retrigger',
            'title': 'Retrigger: original-title',
            'subject': 'Retrigger: original-title',
            'retrigger': 'true',
            # 'send_report_to_upstream': missing
            'skip_rhcheckpatch': 'true',
            # 'mail_to': removed
            # 'mail_cc': removed
            # 'mail_bcc': removed
            # 'mail_add_maintainers_to': removed
            # 'ystream_distro': removed
            # 'ystream_buildroot': removed
            # 'test_priority': removed,
            'skip_beaker': 'true'})

    @responses.activate
    @mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
    def test_variable_cleaning_override(self):
        """Check that it is possible to override all cleaned variables."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki_project')
        self.add_branch(1, 'cki_pipeline_branch')

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, self.variables, variable_overrides={
            'cki_project': 'cki_project',
            'cki_pipeline_branch': 'cki_pipeline_branch',
            'cki_pipeline_type': 'cki_pipeline_type',
            'title': 'title',
            'subject': 'subject',
            'retrigger': 'retrigger',
            'send_report_to_upstream': 'send_report_to_upstream',
            'skip_rhcheckpatch': 'skip_rhcheckpatch',
            'mail_to': 'mail_to',
            'mail_cc': 'mail_cc',
            'mail_bcc': 'mail_bcc',
            'mail_add_maintainers_to': 'mail_add_maintainers_to',
            'ystream_distro': 'ystream_distro',
            'ystream_buildroot': 'ystream_buildroot',
            'test_priority': 'priority',
            'skip_beaker': 'skip_beaker',
        }, is_production=False)
        variables = self._check_variables(is_production=False)
        self.assertEqual(variables, {
            'cki_project': 'cki_project',
            'cki_pipeline_branch': 'cki_pipeline_branch',
            'cki_pipeline_type': 'cki_pipeline_type',
            'title': 'title',
            'subject': 'subject',
            'retrigger': 'retrigger',
            'send_report_to_upstream': 'send_report_to_upstream',
            'skip_rhcheckpatch': 'skip_rhcheckpatch',
            'mail_to': 'mail_to',
            'mail_cc': 'mail_cc',
            'mail_bcc': 'mail_bcc',
            'mail_add_maintainers_to': 'mail_add_maintainers_to',
            'ystream_distro': 'ystream_distro',
            'ystream_buildroot': 'ystream_buildroot',
            'test_priority': 'priority',
            'skip_beaker': 'skip_beaker'})

    @responses.activate
    @mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
    def test_variable_cleaning_patchwork_v1(self):
        """Check cleaning of patchwork v1 variables."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, dict(self.variables, **{
            'cki_pipeline_type': 'patchwork-v1',
            'last_patch_id': '12345',
        }), is_production=False)
        variables = self._check_variables(is_production=False)
        self.assertNotIn('last_patch_id', variables)

    @responses.activate
    @mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
    def test_variable_cleaning_patchwork_v2(self):
        """Check cleaning of patchwork v2 variables."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, dict(self.variables, **{
            'cki_pipeline_type': 'patchwork',
            'event_id': '12345',
        }), is_production=False)
        variables = self._check_variables(is_production=False)
        self.assertNotIn('event_id', variables)

    @responses.activate
    @mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
    def test_variable_cleaning_patchwork_v1_override(self):
        """Check overriding of patchwork v1 variables."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, self.variables, variable_overrides={
            'cki_pipeline_type': 'patchwork-v1',
            'last_patch_id': '12345',
        }, is_production=False)
        variables = self._check_variables(is_production=False)
        self.assertEqual(variables['last_patch_id'], '12345')

    @responses.activate
    @mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
    def test_variable_cleaning_patchwork_v2_override(self):
        """Check overriding of patchwork v2 variables."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, self.variables, variable_overrides={
            'cki_pipeline_type': 'patchwork',
            'event_id': '12345',
        }, is_production=False)
        variables = self._check_variables(is_production=False)
        self.assertEqual(variables['event_id'], '12345')

    @responses.activate
    @mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
    def test_variable_cleaning_patchwork_v1_production(self):
        """Check no cleaning of patchwork v1 variables in production."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, dict(self.variables, **{
            'cki_pipeline_type': 'patchwork-v1',
            'last_patch_id': '12345',
        }), is_production=True)
        variables = self._check_variables(is_production=True)
        self.assertEqual(variables['last_patch_id'], '12345')

    @responses.activate
    @mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
    def test_variable_cleaning_patchwork_v2_production(self):
        """Check no cleaning of patchwork v2 variables in production."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, dict(self.variables, **{
            'cki_pipeline_type': 'patchwork',
            'event_id': '12345',
        }), is_production=True)
        variables = self._check_variables(is_production=True)
        self.assertEqual(variables['event_id'], '12345')

    @responses.activate
    @mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
    def test_variable_cleaning_patchwork_v1_production_forced(self):
        """Check forced cleaning of patchwork v1 variables in production."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, dict(self.variables, **{
            'cki_pipeline_type': 'patchwork-v1',
            'last_patch_id': '12345',
        }), is_production=True, clean_production_vars_tracking=True)
        variables = self._check_variables(is_production=True)
        self.assertNotIn('last_patch_id', variables)

    @responses.activate
    @mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
    def test_variable_cleaning_patchwork_v2_production_forced(self):
        """Check forced cleaning of patchwork v2 variables in production."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, dict(self.variables, **{
            'cki_pipeline_type': 'patchwork',
            'event_id': '12345',
        }), is_production=True, clean_production_vars_tracking=True)
        variables = self._check_variables(is_production=True)
        self.assertNotIn('event_id', variables)
