"""Gitlab api interaction tests."""
import unittest
from unittest import mock

from gitlab import Gitlab
from gitlab.v4 import objects as gitlab_objects
import responses

from cki_lib import gitlab


class PipelineMock:
    # pylint: disable=too-few-public-methods, redefined-builtin, invalid-name
    """PipelineMock."""

    def __init__(self, created_at, id):
        """Initialization."""
        self.created_at = created_at
        self.id = id


class MockedGet:
    """Mock for requests.get."""

    def __init__(self, status_code=200, returns_json=True):
        """Init."""
        self.status_code = status_code
        self.returns_json = returns_json

    def __call__(self, url, **kwds):
        """Fake __call__."""
        return self

    def json(self):
        """Fake json()."""
        if self.returns_json:
            return {}
        raise ValueError

    @property
    def content(self):
        """Fake content."""
        return b''


class TestGitLab(unittest.TestCase):
    """Test GitLab helpers."""

    @staticmethod
    @mock.patch.dict('os.environ', {'GITLAB_TOKENS': '{}'})
    @mock.patch('cki_lib.gitlab.get_token')
    def test_get_instance_get_token(get_token):
        """Check that get_token is used."""
        gitlab.get_instance('https://a')
        get_token.assert_called()

    @staticmethod
    @mock.patch.dict('os.environ', {'GITLAB_TOKENS': '{}'})
    @mock.patch('cki_lib.gitlab.get_token')
    def test_get_instance_no_get_token(get_token):
        """Check that get_token is not used if token is provided."""
        gitlab.get_instance('https://a', token='c')
        get_token.assert_not_called()

    @staticmethod
    @mock.patch.dict('os.environ', {'TEST_TOKENS': '{}'})
    @mock.patch('gitlab.Gitlab')
    def test_get_instance_no_token(mock_gitlab):
        """Check that the provided env name is used."""
        gitlab.get_instance('https://a', env_name='TEST_TOKENS')
        mock_gitlab.assert_called_with(
            'https://a', session=mock.ANY, private_token=None)

    @staticmethod
    @mock.patch.dict('os.environ', {'TEST_TOKENS': '{"a":"b"}'})
    @mock.patch('gitlab.Gitlab')
    def test_get_instance_no_token_var(mock_gitlab):
        """Check that the provided env name is used."""
        gitlab.get_instance('https://a', env_name='TEST_TOKENS')
        mock_gitlab.assert_called_with(
            'https://a', session=mock.ANY, private_token=None)

    @staticmethod
    @mock.patch.dict('os.environ', {'TEST_TOKENS': '{"a":"b"}', 'b': 'c'})
    @mock.patch('gitlab.Gitlab')
    def test_get_instance_custom_tokens(mock_gitlab):
        """Check that the provided env name is used."""
        gitlab.get_instance('https://a', env_name='TEST_TOKENS')
        mock_gitlab.assert_called_with(
            'https://a', session=mock.ANY, private_token='c')


class TestGitLabParseURL(unittest.TestCase):
    """Test parse_gitlab_url."""

    @staticmethod
    def mock_responses():
        """Set up tests."""
        url = 'https://gitlab.com/api/v4/projects'
        mocks = [
            ('/group_foo%2Fproject_foo', {'id': 1}),
            ('/group_foo%2Fproject_foo%2Fsubproject_foo', {'id': 2}),
            ('/group_foo%2Fproject_foo/pipelines/1234', {'id': 1234}),
            ('/group_foo%2Fproject_foo%2Fsubproject_foo/pipelines/1234', {'id': 1234}),
            ('/group_foo%2Fproject_foo/merge_requests/4321', {'id': 4321, 'iid': 12}),
            ('/group_foo%2Fproject_foo%2Fsubproject_foo/merge_requests/4321',
             {'id': 4321, 'iid': 12}),
            ('/group_foo%2Fproject_foo/merge_requests/12/notes/1122', {'id': 1122}),
            ('/group_foo%2Fproject_foo%2Fsubproject_foo/merge_requests/12/notes/1122',
             {'id': 1122}),
            ('/group_foo%2Fproject_foo/pipeline_schedules/9', {'id': 1}),
            ('/group_foo%2Fproject_foo%2Fsubproject_foo/pipeline_schedules/9', {'id': 2}),
            ('/group_foo%2Fproject_foo/issues/1234', {'id': 1234}),
            ('/group_foo%2Fproject_foo%2Fsubproject_foo/issues/1234', {'id': 1234}),
        ]

        for endpoint, payload in mocks:
            responses.add(responses.GET, url + endpoint, json=payload)

    @responses.activate
    def test_get_pipeline(self):
        """Test parse_gitlab_url with pipeline urls."""
        self.mock_responses()
        urls = [
            'https://gitlab.com/group_foo/project_foo/-/pipelines/1234',
            'https://gitlab.com/group_foo/project_foo/subproject_foo/-/pipelines/1234',
        ]
        for url in urls:
            gl_instance, obj = gitlab.parse_gitlab_url(url)

            self.assertIsInstance(gl_instance, Gitlab)
            self.assertIsInstance(obj, gitlab_objects.ProjectPipeline)
            self.assertEqual(1234, obj.id)

    @responses.activate
    def test_get_merge_request(self):
        """Test parse_gitlab_url with MR urls."""
        self.mock_responses()
        urls = [
            'https://gitlab.com/group_foo/project_foo/-/merge_requests/4321',
            'https://gitlab.com/group_foo/project_foo/subproject_foo/-/merge_requests/4321',
        ]
        for url in urls:
            gl_instance, obj = gitlab.parse_gitlab_url(url)

            self.assertIsInstance(gl_instance, Gitlab)
            self.assertIsInstance(obj, gitlab_objects.ProjectMergeRequest)
            self.assertEqual(4321, obj.id)

    @responses.activate
    def test_get_merge_request_note(self):
        """Test parse_gitlab_url with MR Note urls."""
        self.mock_responses()
        urls = [
            'https://gitlab.com/group_foo/project_foo/-/merge_requests/4321#note_1122',
            'https://gitlab.com/group_foo/project_foo/subproject_foo/-/merge_requests/4321'
            '#note_1122',
        ]
        for url in urls:
            gl_instance, obj = gitlab.parse_gitlab_url(url)

            self.assertIsInstance(gl_instance, Gitlab)
            self.assertIsInstance(obj, gitlab_objects.ProjectMergeRequestNote)
            self.assertEqual(1122, obj.id)

    @responses.activate
    def test_get_project(self):
        """Test parse_gitlab_url with Project urls."""
        self.mock_responses()
        urls = [
            ('https://gitlab.com/group_foo/project_foo', 1),
            ('https://gitlab.com/group_foo/project_foo/subproject_foo', 2),
        ]
        for url, id in urls:
            gl_instance, obj = gitlab.parse_gitlab_url(url)

            self.assertIsInstance(gl_instance, Gitlab)
            self.assertIsInstance(obj, gitlab_objects.Project)
            self.assertEqual(id, obj.id)

    @responses.activate
    def test_get_schedule(self):
        """Test parse_gitlab_url with pipeline schedule urls."""
        self.mock_responses()
        urls = [
            ('https://gitlab.com/group_foo/project_foo/-/pipeline_schedules/9', 1),
            ('https://gitlab.com/group_foo/project_foo/subproject_foo/-/pipeline_schedules/9', 2),
        ]
        for url, id in urls:
            gl_instance, obj = gitlab.parse_gitlab_url(url)

            self.assertIsInstance(gl_instance, Gitlab)
            self.assertIsInstance(obj, gitlab_objects.ProjectPipelineSchedule)
            self.assertEqual(id, obj.id)

    @responses.activate
    def test_get_issue(self):
        """Test parse_gitlab_url with issue urls."""
        self.mock_responses()
        urls = [
            'https://gitlab.com/group_foo/project_foo/-/issues/1234',
            'https://gitlab.com/group_foo/project_foo/subproject_foo/-/issues/1234',
        ]
        for url in urls:
            gl_instance, obj = gitlab.parse_gitlab_url(url)

            self.assertIsInstance(gl_instance, Gitlab)
            self.assertIsInstance(obj, gitlab_objects.ProjectIssue)
            self.assertEqual(1234, obj.id)
