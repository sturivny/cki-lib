"""Glue logic interaction tests."""
import unittest

from cki_lib import glue


class TestGlue(unittest.TestCase):
    """Test glue class."""

    def setUp(self) -> None:
        self.test_xml = """<whiteboard>skt ##KVER## [noavc]
        [noselinux]</whiteboard>
              <task name="/distribution/kpkginstall" role="STANDALONE">
        <fetch url="url"/>
        <params>
          <param name="KPKG_URL" value="##KPKG_URL##"/>
          <param name="KVER" value="##KVER##"/>
        </params>
        </task>
        <and>
          <arch op="=" value="##ARCH##"/>
        </and>"""

    def test_getxml_replace(self):
        """Ensure BeakerRunner.__getxml() returns xml with replacements."""
        # pylint: disable=protected-access,no-member
        result = glue.do_xml_replacements(self.test_xml.splitlines(),
                                          {'KVER': 'kernel-4.16'})
        expected_xml = self.test_xml.replace("##KVER##", "kernel-4.16")
        self.assertEqual(result, expected_xml)

    def test_getxml_multi_replace(self):
        """
        Ensure BeakerRunner.__getxml() returns xml with multi-instance
        replacements.
        """
        # pylint: disable=protected-access,no-member
        result = glue.do_xml_replacements(self.test_xml.splitlines(),
                                          {'ARCH': 's390x'})
        expected_xml = self.test_xml.replace("##ARCH##", "s390x")
        self.assertEqual(result, expected_xml)

    def test_do_xml_replacements(self):
        """Ensure BeakerRunner.__getxml() returns xml."""
        # pylint: disable=protected-access,no-member
        result = glue.do_xml_replacements(self.test_xml.splitlines(), {})
        self.assertEqual(self.test_xml, result)

    def test_do_xml_replacements_invalid_replace(self):
        """
        Ensure BeakerRunner.__getxml() raises ValueError if the replacement is
        not a string.
        """
        # pylint: disable=protected-access,no-member
        with self.assertRaises(ValueError):
            line_lst = ['<xml> ###KVER### <xml/>']
            glue.do_xml_replacements(line_lst, {"KVER": None})
