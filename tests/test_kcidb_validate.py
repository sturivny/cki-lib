import copy
import json
import pathlib
import tempfile
import unittest
from unittest import mock

import jsonschema

from cki_lib.kcidb import validate

JOB_DATA = {
    'id': 1234,
}
PIPELINE_DATA = {
    'id': 1234,
    'ref': 'reference',
    'project': {
        'id': 1,
        'path_with_namespace': 'project/path',
        'instance_url': 'http://gitlab.com',
    },
    'variables': {
        'key': 'value',
    }

}
CHECKOUT_DATA = {
    'id': 'redhat:1234',
    'origin': 'redhat',
    'misc': {
        'retrigger': False,
        'scratch': False,
        'job': JOB_DATA,
        'pipeline': PIPELINE_DATA,
        'actions': {
            'last_triaged_at': '2021-09-01T12:44:41.661629Z',
        },
        'patchset_modified_files': [
            {'path': 'file_a'},
            {'path': 'file_b'},
        ],
        'brew_task_id': 123,
        'submitter': 'Someone <someone@mail.com>',
        'mr': {
            'id': 1,
            'url': 'https://mr.url',
            'diff_url': 'https://mr.url/diff'
        },
        'send_ready_for_test_pre': True,
        'send_ready_for_test_post': False,
        'kernel_version': '1.2.3.4',
        'all_sources_targeted': True,
        'report_rules': [
            {'when': 'condition', 'send_to': 'some_group', 'send_bcc': 'some_other_group'},
            {'when': 'condition', 'send_cc': 'some_group'},
            {'when': 'condition', 'send_bcc': 'some_other_group'},
            {'when': 'condition',
             'send_to': ['recipient_a', 'recipient_b'],
             'send_cc': ['recipient_a', 'recipient_b'],
             'send_bcc': ['recipient_a', 'recipient_b']},
        ]

    }
}
BUILD_DATA = {
    'id': 'redhat:1234_build',
    'checkout_id': 'redhat:1234',
    'origin': 'redhat',
    'misc': {
        'job': JOB_DATA,
        'pipeline': PIPELINE_DATA,
        'debug': False,
        'actions': {
            'last_triaged_at': '2021-09-01T12:44:41.661629Z',
        },
        'test_plan_missing': False,
        'package_name': 'kernel',
        'testing_skipped_reason': 'unsupported',
        'kpet_tree_name': 'name',
        'kpet_tree_family': 'name',
    }
}
TEST_DATA = {
    'id': 'redhat:1234_test',
    'build_id': 'redhat:1234_build',
    'origin': 'redhat',
    'misc': {
        'job': JOB_DATA,
        'pipeline': PIPELINE_DATA,
        'beaker': {
            'finish_time': '2021-09-01T12:44:41.661629Z',
            'recipe_id': 123,
            'retcode': 0,
            'task_id': 234,
        },
        'debug': True,
        'fetch_url': 'http://fetch.url',
        'targeted': True,
        'rerun_index': 1,
        'maintainers': [
            {'name': 'Maint 1', 'email': 'maint_1@mail.com'},
            {'name': 'Maint 1', 'email': 'maint_1@mail.com', 'gitlab': 'maint_1'},
        ],
        'actions': {
            'last_triaged_at': '2021-09-01T12:44:41.661629Z',
        },
        'results': [
            {'id': 'r:1', 'name': '/test 1 2 3', 'status': 'PASS'},
            {'id': 'r:2', 'name': '/test 4 5 6', 'status': 'FAIL',
             'output_files': [{'name': 'log', 'url': 'http://file'}]},
        ],
        'forced_skip_status': True,
    }
}
VALID_DATA = {
    'version': {'major': 4, 'minor': 0},
    'checkouts': [
        CHECKOUT_DATA,
    ],
    'builds': [
        BUILD_DATA,
    ],
    'tests': [
        TEST_DATA,
    ]
}


class TestSchema(unittest.TestCase):
    """Test schema validation."""

    @staticmethod
    def test_valid_schema():
        """Test schema is valid."""
        validate.validate(VALID_DATA)

    @staticmethod
    def test_valid_complete_schema():
        """Test schema is valid."""
        validate.validate(
            {'version': {'major': 4, 'minor': 0}}
        )

        validate.validate(
            {'version': {'major': 4, 'minor': 0},
             'checkouts': [CHECKOUT_DATA]}
        )

        validate.validate(
            {'version': {'major': 4, 'minor': 0},
             'builds': [BUILD_DATA]}
        )

        validate.validate(
            {'version': {'major': 4, 'minor': 0},
             'tests': [TEST_DATA]}
        )

    def test_invalid(self):
        """Test invalid schema."""
        invalid_checkout = copy.deepcopy(CHECKOUT_DATA)
        del invalid_checkout['misc']['job']
        self.assertRaises(
            Exception,
            validate.validate,
            {'version': {'major': 4, 'minor': 0},
             'checkouts': [invalid_checkout]}
        )

    def test_invalid_upstream(self):
        """Test invalid upstream schema."""
        self.assertRaises(
            Exception,
            validate.validate,
            {'version': {'major': 4, 'minor': 0},
             'checkouts': [{}]}
        )


class TestValidate(unittest.TestCase):
    """Test validation methods."""

    def setUp(self):
        """Set up json file."""
        self.file_path = tempfile.NamedTemporaryFile()
        self.file = pathlib.Path(self.file_path.name)
        self.file.write_text(
            json.dumps(VALID_DATA)
        )

    @mock.patch('cki_lib.kcidb.validate.validate')
    def test_cli(self, mock_validate):
        """Test cli call."""
        validate.main([self.file_path.name])
        mock_validate.assert_called_with(VALID_DATA)

    @staticmethod
    @mock.patch('cki_lib.kcidb.validate.jsonschema.validate')
    @mock.patch('cki_lib.kcidb.validate.kcidb_io.schema.validate')
    def test_validate(mock_kcidb_validate, mock_validate):
        """Test validate calls both upstream and cki validate."""
        validate.validate(VALID_DATA)
        mock_kcidb_validate.assert_called_with(VALID_DATA)
        mock_validate.assert_called_with(
            instance=VALID_DATA,
            schema=validate.SCHEMA,
            format_checker=jsonschema.draft7_format_checker
        )
