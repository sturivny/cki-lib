"""logger tester."""
import datetime
import io
import json
import logging
import os
import pathlib
import tempfile
import threading
import unittest
from unittest import mock

from freezegun import freeze_time

from cki_lib import logger


class TestLogger(unittest.TestCase):
    """Test logger class."""

    def test_cki_prepended(self):
        """test cki prefix added to name if missing from passed name"""
        mod_a_logger = logger.get_logger('mod_a')
        self.assertEqual(mod_a_logger.name, 'cki.mod_a')

    def test_truncate_logs(self):
        """Ensure truncate_logs works."""
        with tempfile.TemporaryDirectory() as tempdir, \
                mock.patch('cki_lib.logger.CKI_LOGS_FILEDIR', tempdir):
            dst_file = pathlib.Path(logger.CKI_LOGS_FILEDIR, 'unittesting.log')
            dst_file.write_bytes(b'foo')

            # empty all logfiles + check logfile we just changed is now empty
            logger.truncate_logs()

            self.assertEqual(dst_file.read_bytes(), b'')


class TestLoggerPatched(unittest.TestCase):
    """Test logger class for methods that need STREAM patched."""

    @classmethod
    def reset_logger(self):
        """remove all handlers from cki logger to allow simulating initial
        creation"""
        logger.STREAM = io.StringIO()
        logging.getLogger().handlers = []
        logger.CKI_HANDLER = False
        self.mock_stderr = logger.STREAM

    def setUp(self):
        """Set up logger stream."""
        self.original_stream = logger.STREAM
        self.reset_logger()

    def tearDown(self):
        """Restore logger stream."""
        logger.STREAM = self.original_stream

    def test_propagation(self):
        """test inheritance and propagation"""
        cki_logger = logger.get_logger('cki')
        mod_a_logger = logger.get_logger('cki.mod_a')
        mod_b_logger = logger.get_logger('cki.mod_b')

        # root will default to logging.WARNING
        cki_logger.setLevel(logging.WARNING)
        mod_a_logger.setLevel(logging.CRITICAL)
        mod_b_logger.setLevel(logging.DEBUG)

        # cki logs warning but not DEBUG
        cki_logger.warning('test_propagation')
        self.assertTrue(' - [WARNING] - cki - test_propagation'
                        in self.mock_stderr.getvalue())
        self.mock_stderr.truncate(0)
        cki_logger.debug('cki_logger')
        self.assertEqual(self.mock_stderr.getvalue(), '')
        self.mock_stderr.truncate(0)

        # mod_a doesn't log WARNING
        mod_a_logger.warning('mod_a_logger')
        self.assertEqual(self.mock_stderr.getvalue(), '')
        self.mock_stderr.truncate(0)

        # mod_b logs DEBUG
        mod_b_logger.debug('mod_b_logger')
        self.assertTrue(' - [DEBUG] - cki.mod_b - mod_b_logger'
                        in self.mock_stderr.getvalue())

    def test_handlers(self):
        """test only one handler is created"""
        logger1 = logger.get_logger('logger1')
        logger.get_logger('logger2')
        logger1.setLevel(logging.WARNING)
        logger1.warning('logger1')
        self.assertTrue(self.mock_stderr.getvalue().count('\n') == 1)

    def test_env_config(self):
        """test default logging level and that CKI_LOGGING_LEVEL changes it"""
        # defaults to WARNING
        cki_logger = logger.get_logger('cki')
        cki_logger.warning('test_env_config')
        self.assertTrue(' - [WARNING] - cki - test_env_config'
                        in self.mock_stderr.getvalue())
        self.mock_stderr.truncate(0)
        cki_logger.info('test_env_config')
        self.assertEqual(self.mock_stderr.getvalue(), '')

        # CKI_LOGGING_LEVEL changes level if cki logger recreated
        self.reset_logger()
        with mock.patch.dict(os.environ, {'CKI_LOGGING_LEVEL': 'INFO'}):
            cki_logger = logger.get_logger('cki')
        cki_logger.info('test_env_config')
        self.assertTrue(' - [INFO] - cki - test_env_config'
                        in self.mock_stderr.getvalue())

    @mock.patch.dict(os.environ, {'CKI_LOGGING_FORMAT': 'json'})
    def test_json_threading(self):
        """Test json logging of extra data is thread safe."""
        barrier = threading.Barrier(2)

        def _thread() -> None:
            barrier.wait()  # 1
            cki_logger.warning('2 - thread, before')
            barrier.wait()  # 2
            barrier.wait()  # 3
            cki_logger.warning('4 - thread, in env')
            barrier.wait()  # 4
            barrier.wait()  # 5
            cki_logger.warning('6 - thread, after')
            barrier.wait()  # 6

        # defaults to WARNING
        cki_logger = logger.get_logger('cki')
        threading.Thread(target=_thread).start()

        cki_logger.warning('1 - main, before')
        barrier.wait()  # 1
        barrier.wait()  # 2
        with logger.logging_env({'foo': 'bar'}):
            cki_logger.warning('3 - main, in env')
            barrier.wait()  # 3
            barrier.wait()  # 4
        cki_logger.warning('5 - main, after')
        barrier.wait()  # 5
        barrier.wait()  # 6

        data = [json.loads(s) for s in self.mock_stderr.getvalue().split('\n') if s.startswith('{')]
        self.assertEqual(list(zip((d['message'][0] for d in data), (d['extras'] for d in data))), [
            ('1', {}), ('2', {}),
            ('3', {'foo': 'bar'}), ('4', {}),
            ('5', {}), ('6', {}),
        ])

    @freeze_time("2010-01-02 00:00:00")
    @mock.patch.dict(os.environ, {'CKI_LOGGING_FORMAT': 'json'})
    def test_json(self):
        """Test json logging of extra data."""
        class _BadStr:  # pylint: disable=too-few-public-methods
            def __str__(self) -> str:
                raise Exception('foo')

        cki_logger = logger.get_logger('cki')
        with logger.logging_env({
            'foo': 'bar',
            'baz': datetime.datetime(1970, 1, 1),
            'qux': _BadStr(),
        }):
            cki_logger.warning('something happened')

        self.assertDictEqual(
            {
                'timestamp': 1262390400.0,
                'logger': {'level': 'WARNING', 'name': 'cki'},
                'message': 'something happened',
                'extras': {'foo': 'bar', 'baz': '1970-01-01T00:00:00', 'qux': 'invalid: _BadStr'},
            },
            json.loads(self.mock_stderr.getvalue())
        )

    @freeze_time("2010-01-02 00:00:00")
    @mock.patch.dict(os.environ, {'CKI_LOGGING_FORMAT': 'pack'})
    def test_pack(self):
        """Test pack logging."""
        class _BadStr:  # pylint: disable=too-few-public-methods
            def __str__(self) -> str:
                raise Exception('foo')

        cki_logger = logger.get_logger('cki')
        with logger.logging_env({
            'str': 'foo',
            'int': 5,
            'list': ['bar', 'baz'],
            'dict': {'qux': 'quux'},
            'nested_weirdo': {'foo': [{'bar': 'baz'}, {'bar': 'baz'}]},
            'set': {'quuz'},
            'none': None,
            'date': datetime.datetime(1970, 1, 1),
            'bad': _BadStr(),
        }):
            cki_logger.warning('something happened')

        self.assertDictEqual(json.loads(self.mock_stderr.getvalue()), {
            'timestamp': '1262390400.0',
            'logger_name': 'cki',
            'logger_level': 'WARNING',
            '_entry': 'something happened',
            'extras_str': 'foo',
            'extras_int': '5',
            'extras_list_0': 'bar',
            'extras_list_1': 'baz',
            'extras_dict_qux': 'quux',
            'extras_nested_weirdo_foo_0_bar': 'baz',
            'extras_nested_weirdo_foo_1_bar': 'baz',
            'extras_set': "{'quuz'}",
            'extras_none': 'None',
            'extras_date': '1970-01-01T00:00:00',
            'extras_bad': 'invalid: _BadStr',
        })

    def test_logging_env(self):
        """
        Test logging_env helper.

        Ensure values are correctly set and unset.
        """
        self.assertFalse('foo' in logger.LOGGING_ENV.__dict__)

        with logger.logging_env({'foo': 'bar'}):
            self.assertEqual('bar', logger.LOGGING_ENV.__dict__['foo'])

        self.assertFalse('foo' in logger.LOGGING_ENV.__dict__)
