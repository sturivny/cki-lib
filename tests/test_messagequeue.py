"""Test MessageQueue."""
from contextlib import suppress
import itertools
import json
import os
import platform
import signal
import ssl
import threading
import time
import unittest
from unittest import mock

import pika

from cki_lib import logger
from cki_lib import messagequeue
from cki_lib import misc
from cki_lib import session

LOGGER = logger.get_logger(__name__)
SESSION = session.get_session(__name__)


class TestMessageQueue(unittest.TestCase):
    # pylint: disable=too-many-public-methods
    """Test cki_lib/messagequeue.py."""

    def test_init(self):
        """Test MessageQueue init."""
        queue = messagequeue.MessageQueue(host='host/', port=123, virtual_host='vhost',
                                          user='user', password='password')
        credentials = pika.PlainCredentials('user', 'password')
        connection_params = [pika.ConnectionParameters(
            host='host', port=123, virtual_host='vhost',
            credentials=credentials,
            client_properties={'connection_name': platform.node()},
        )]

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)

    @mock.patch.dict(os.environ, {
        'RABBITMQ_HOST': 'host',
        'RABBITMQ_PORT': '123',
        'RABBITMQ_VIRTUAL_HOST': 'vhost',
        'RABBITMQ_USER': 'user',
        'RABBITMQ_PASSWORD': 'password',
    })
    def test_init_env_default(self):
        """Test MessageQueue default values from env variables."""
        queue = messagequeue.MessageQueue()
        credentials = pika.PlainCredentials('user', 'password')
        connection_params = [pika.ConnectionParameters(
            host='host', port=123, virtual_host='vhost',
            credentials=credentials,
            client_properties={'connection_name': platform.node()},
        )]

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)

    @mock.patch.dict(os.environ, {
        'RABBITMQ_HOST': 'host-ignored',
        'RABBITMQ_PORT': '1234',
        'RABBITMQ_VIRTUAL_HOST': 'vhost-ignored',
        'RABBITMQ_USER': 'user-ignored',
        'RABBITMQ_PASSWORD': 'password-ignored',
    })
    def test_init_env_default_ignored(self):
        """Test MessageQueue ignoring default values from env variables."""
        queue = messagequeue.MessageQueue(host='host', port=123, virtual_host='vhost',
                                          user='user', password='password')
        credentials = pika.PlainCredentials('user', 'password')
        connection_params = [pika.ConnectionParameters(
            host='host', port=123, virtual_host='vhost',
            credentials=credentials,
            client_properties={'connection_name': platform.node()},
        )]

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)

    def test_init_default(self):
        """Test MessageQueue init default values."""
        queue = messagequeue.MessageQueue(host='host')
        credentials = pika.PlainCredentials('guest', 'guest')
        connection_params = [pika.ConnectionParameters(
            host='host', port=5672, virtual_host='/',
            credentials=credentials,
            client_properties={'connection_name': platform.node()},
        )]

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)

    def test_init_ssl_443(self):
        """Test MessageQueue init default values with ssl."""
        queue = messagequeue.MessageQueue(host='host', port=443)
        credentials = pika.PlainCredentials('guest', 'guest')
        ssl_options = pika.SSLOptions(ssl.create_default_context())
        connection_params = [pika.ConnectionParameters(
            host='host', port=443,  virtual_host='/',
            credentials=credentials,
            client_properties={'connection_name': platform.node()},
            ssl_options=ssl_options
        )]

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)

    def test_init_ssl_5671(self):
        """Test MessageQueue init default values with ssl."""
        queue = messagequeue.MessageQueue(host='host', port=5671)
        credentials = pika.PlainCredentials('guest', 'guest')
        ssl_options = pika.SSLOptions(ssl.create_default_context())
        connection_params = [pika.ConnectionParameters(
            host='host', port=5671, virtual_host='/',
            credentials=credentials,
            client_properties={'connection_name': platform.node()},
            ssl_options=ssl_options
        )]

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)

    @mock.patch('ssl.SSLContext.load_cert_chain')
    def test_init_ssl_cert(self, load_cert_chain):
        """Test MessageQueue init with ssl cert."""
        certfile = 'certfile'
        queue = messagequeue.MessageQueue(
            host='host', port=443, certfile=certfile)
        credentials = pika.credentials.ExternalCredentials()
        ssl_options = pika.SSLOptions(ssl.create_default_context())
        connection_params = [pika.ConnectionParameters(
            host='host', port=443, virtual_host='/',
            credentials=credentials,
            client_properties={'connection_name': platform.node()},
            ssl_options=ssl_options
        )]

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)
        load_cert_chain.assert_called_with(certfile)

    def test_init_ssl_custom(self):
        """Test MessageQueue init default values with custom ssl."""
        ssl_options = pika.SSLOptions(ssl.create_default_context(), 'host')
        queue = messagequeue.MessageQueue(
            host='host', port=443, connection_params={'ssl_options': ssl_options})
        credentials = pika.PlainCredentials('guest', 'guest')
        connection_params = [pika.ConnectionParameters(
            host='host', port=443, virtual_host='/',
            credentials=credentials,
            client_properties={'connection_name': platform.node()},
            ssl_options=ssl_options
        )]

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)

    @staticmethod
    def _sorted_connections(connections):
        return sorted(connections, key=lambda c: c.host)

    def _test_init_multiple(self, param, hosts):
        queue = messagequeue.MessageQueue(host=param)
        credentials = pika.PlainCredentials('guest', 'guest')
        connection_params = [pika.ConnectionParameters(
            host=h, port=5672, virtual_host='/',
            credentials=credentials,
            client_properties={'connection_name': platform.node()},
        ) for h in hosts]

        self.assertEqual(queue.credentials, credentials)
        self.assertEqual(queue.connection_params, connection_params)

        with mock.patch('pika.BlockingConnection') as connection:
            with queue.connect():
                pass
        seen = self._sorted_connections(connection.mock_calls[0][1][0])
        self.assertEqual(seen, self._sorted_connections(connection_params))

    def test_init_array(self):
        """Test MessageQueue with a host array."""
        hosts = [f'host{i}' for i in range(32)]
        self._test_init_multiple(hosts, hosts)

    def test_init_string_multiple_space(self):
        """Test MessageQueue with a space-delimited string."""
        hosts = [f'host{i}' for i in range(32)]
        self._test_init_multiple(' '.join(hosts), hosts)

    def test_init_string_multiple_spaces(self):
        """Test MessageQueue with a multi-space-delimited string."""
        hosts = [f'host{i}' for i in range(32)]
        self._test_init_multiple('  '.join(hosts), hosts)

    def test_init_string_multiple_lf(self):
        """Test MessageQueue with a LF-delimited string."""
        hosts = [f'host{i}' for i in range(32)]
        self._test_init_multiple('\n'.join(hosts), hosts)

    @mock.patch('pika.BlockingConnection')
    def test_connect(self, blockingconnection):
        # pylint: disable=no-self-use
        """Test connection parameters."""
        queue = messagequeue.MessageQueue(host='host', port=123,
                                          user='user', password='password')

        with queue.connect():
            blockingconnection.assert_called_with(queue.connection_params)

    @mock.patch('pika.ConnectionParameters')
    def test_connection_params(self, connection_params):
        # pylint: disable=no-self-use
        """Test connection params kwarg on init."""
        queue = messagequeue.MessageQueue(
            host='host', port=123, user='user', password='password',
            connection_params={'extraparam': 'value',
                               'someotherparam': 'othervalue'}
        )
        connection_params.assert_called_with(
            credentials=queue.credentials, host='host', port=123, virtual_host='/',
            client_properties={'connection_name': platform.node()},
            extraparam='value', someotherparam='othervalue'
        )

    def test_connect_context_manager(self):
        # pylint: disable=protected-access
        """Test connect with and without keepalive."""
        queue = messagequeue.MessageQueue(host='host', port=123,
                                          user='user', password='password')
        queue._connect_no_keepalive = mock.Mock()
        queue._connect_and_keepalive = mock.Mock()

        queue.keepalive_s = 0
        queue.connect()
        self.assertTrue(queue._connect_no_keepalive.called)

        queue.keepalive_s = 1
        queue.connect()
        self.assertTrue(queue._connect_and_keepalive.called)

    @mock.patch('pika.BlockingConnection')
    def test_connect_context_manager_no_keepalive(self, connection):
        # pylint: disable=protected-access
        """Test context manager."""
        queue = messagequeue.MessageQueue(host='host', port=123,
                                          user='user', password='password')

        with queue.connect():
            connection.assert_called_with(queue.connection_params)
        self.assertTrue(connection().close.called)

    @mock.patch('pika.BlockingConnection')
    def test_connect_context_manager_keepalive(self, connection):
        # pylint: disable=protected-access
        """Test context manager."""
        queue = messagequeue.MessageQueue(host='host', port=123,
                                          user='user', password='password',
                                          keepalive_s=0.001)
        queue._disconnect_timer = mock.Mock()

        with queue.connect():
            connection.assert_called_with(queue.connection_params)
        self.assertTrue(queue._disconnect_timer.start.called)

    @mock.patch('pika.BlockingConnection')
    def test_connect_keepalive(self, connection):
        """Test keepalive saves connection calls."""
        queue = messagequeue.MessageQueue(host='host', port=123,
                                          user='user', password='password',
                                          keepalive_s=0.1)

        self.assertEqual(0, connection.call_count)
        with queue.connect():
            pass
        self.assertEqual(1, connection.call_count)
        with queue.connect():
            pass
        self.assertEqual(1, connection.call_count)
        time.sleep(0.2)
        with queue.connect():
            pass
        self.assertEqual(2, connection.call_count)

    @mock.patch('pika.BlockingConnection')
    def test_connect_keepalive_exceptions(self, connection):
        # pylint: disable=protected-access
        """Test keepalive disconnects on exceptions."""
        queue = messagequeue.MessageQueue(host='host', port=123,
                                          user='user', password='password',
                                          keepalive_s=10)
        queue.send_message({'key': 'value'}, 'queue_name')
        self.assertIsNotNone(queue._channel)
        connection().channel().basic_publish.side_effect = Exception('boom')
        self.assertRaises(Exception, queue.send_message, {'key': 'value'}, 'queue_name')
        self.assertIsNone(queue._channel)

    def test_disconnect(self):
        # pylint: disable=protected-access
        """Test _disconnect call."""
        queue = messagequeue.MessageQueue(host='host', port=123,
                                          user='user', password='password')
        channel = mock.Mock()
        queue._channel = channel
        queue._disconnect()
        self.assertTrue(channel.connection.close.called)
        self.assertIsNone(queue._channel)

    @staticmethod
    def test_disconnect_reentrant():
        # pylint: disable=protected-access
        """Test _disconnect call."""
        queue = messagequeue.MessageQueue(host='host', port=123,
                                          user='user', password='password')
        queue.connect()
        queue._disconnect()
        queue._disconnect()

    @mock.patch('pika.BlockingConnection', mock.Mock())
    def test_disconnect_timer(self):
        # pylint: disable=protected-access
        """Test _disconnect_timer disconnects."""
        queue = messagequeue.MessageQueue(host='host', port=123,
                                          user='user', password='password',
                                          keepalive_s=0.1)
        with queue.connect():
            pass

        # As _disconnect_timer is created at initialization,
        # _disconnect_timer is not simple to mock. Instead of checking
        # _disconnect_timer call, check for the result of it.
        self.assertIsNotNone(queue._channel)
        time.sleep(0.2)
        self.assertIsNone(queue._channel)

    @mock.patch('pika.BlockingConnection')
    def test_queue_init(self, connection):  # pylint: disable=no-self-use
        """Test queue_init."""
        queue = messagequeue.MessageQueue(host='host', port=123,
                                          user='user', password='password')

        queue.queue_init('queue_name')
        connection().channel().queue_declare.assert_called_with('queue_name')

        queue.queue_init('queue_name', params={'durable': True})
        connection().channel().queue_declare.assert_called_with('queue_name',
                                                                durable=True)

    @mock.patch('pika.BlockingConnection')
    def test_send_message(self, connection):  # pylint: disable=no-self-use
        """Test send_message."""
        data = {'key': 'value'}
        queue_name = 'queue_name'
        exchange_name = 'exchange_name'
        headers = {'header-key': 'value'}
        queue = messagequeue.MessageQueue(host='host', port=123,
                                          user='user', password='password')
        properties = pika.BasicProperties(delivery_mode=2,
                                          headers=headers)

        queue.send_message(
            data, queue_name,
            exchange=exchange_name,
            headers=headers,
        )
        connection().channel().basic_publish.assert_called_with(
            body=json.dumps(data),
            exchange=exchange_name,
            routing_key=queue_name,
            properties=properties,
        )

        # Test default exchange.
        queue.send_message(data, queue_name, headers=headers)
        connection().channel().basic_publish.assert_called_with(
            body=json.dumps(data), exchange='', routing_key=queue_name,
            properties=properties,
        )

    @mock.patch('pika.BlockingConnection')
    def test_consume(self, connection):
        """Test consume_messages."""
        self._test_consume(connection)

    @mock.patch('pika.BlockingConnection')
    def test_consume_kwargs(self, connection):
        """Test consume_messages kwargs callback calls."""
        self._test_consume(connection, callback_kwargs=True)

    @mock.patch('pika.BlockingConnection')
    def test_consume_prefetch(self, connection):
        """Test consume_messages."""
        self._test_consume(connection, prefetch_count=10)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('pika.BlockingConnection')
    def test_consume_production_no_queue(self, connection):
        """Test consume_messages."""
        self._test_consume(connection, is_production=True)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('pika.BlockingConnection')
    def test_consume_production_queue(self, connection):
        """Test consume_messages."""
        self._test_consume(connection, queue_name='queue',
                           is_production=True)

    @mock.patch('pika.BlockingConnection')
    def test_consume_no_production_queue(self, connection):
        """Test consume_messages."""
        self._test_consume(connection, queue_name='queue',
                           is_production=False)

    @mock.patch('pika.BlockingConnection')
    def test_consume_inactivity_timeout(self, connection):
        """Test consume_messages."""
        self._test_consume(connection, inactivity_timeout=60)

    @mock.patch('pika.BlockingConnection')
    def test_consume_inactivity_timeout_no_return(self, connection):
        """Test consume_messages. Inactivity timeout does not return."""
        self._test_consume(connection, inactivity_timeout=60,
                           return_on_timeout=False)

    @mock.patch('pika.BlockingConnection')
    def test_consume_inactivity_timeout_no_return_kwargs(self, connection):
        """Test consume_messages with callback kwargs. Inactivity timeout does not return."""
        self._test_consume(connection, inactivity_timeout=60,
                           return_on_timeout=False,
                           callback_kwargs=True)

    @staticmethod
    @mock.patch('pika.BlockingConnection')
    def test_consume_messages_routing_key(connection):
        """Test consume_messages routing_key allows str type."""
        queue = messagequeue.MessageQueue(host='host', port=123,
                                          user='user', password='password')
        queue.consume_messages('exchange', 'routing', None)
        connection().channel().queue_bind.assert_has_calls([
            mock.call(mock.ANY, mock.ANY, routing_key='routing')
        ])

    @mock.patch('pika.BlockingConnection')
    def test_consume_messages_terminate_consumer(self, connection):
        """Test the main thread signal handling during consuming."""
        connection().channel().consume.return_value = itertools.repeat(
            (mock.Mock(routing_key='a', delivery_tag='b'),
             pika.BasicProperties(), json.dumps({'c': 'd'})))
        callback = mock.Mock(side_effect=lambda a, b: time.sleep(5))
        queue = messagequeue.MessageQueue(host='host', port=123,
                                          user='user', password='password')
        pid = os.getpid()
        threading.Thread(target=lambda: time.sleep(0.1) or
                         os.kill(pid, signal.SIGINT)).start()
        with self.assertRaises(KeyboardInterrupt):
            queue.consume_messages('exchange', 'routing', callback)

    @staticmethod
    def _failing_consume():
        for _ in range(3):
            yield (mock.Mock(routing_key='a', delivery_tag='b'), pika.BasicProperties(),
                   json.dumps({'c': 'd'}))
        time.sleep(0.1)
        raise Exception('boom')

    @mock.patch('pika.BlockingConnection')
    def test_consume_messages_terminate_producer(self, connection):
        """Test the handling of an error in consume."""
        connection().channel().consume.return_value = self._failing_consume()
        queue = messagequeue.MessageQueue(host='host', port=123,
                                          user='user', password='password')
        callback = mock.Mock()
        queue.consume_messages('exchange', 'routing', callback)
        self.assertEqual(len(callback.mock_calls), 3)

    @mock.patch('os._exit')
    @mock.patch('cki_lib.messagequeue.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('cki_lib.messagequeue.misc.sentry_flush')
    @mock.patch('pika.BlockingConnection')
    def test_consume_messages_terminate_producer_production(self, connection, s_flush, os_exit):
        """Test the handling of an error in consume in production mode."""
        connection().channel().consume.return_value = self._failing_consume()
        queue = messagequeue.MessageQueue(host='host', port=123,
                                          user='user', password='password')
        queue.consume_messages('exchange', 'routing', mock.Mock(), queue_name='queue_name')
        s_flush.assert_called()
        os_exit.assert_called()

    @mock.patch('pika.BlockingConnection')
    def test_consume_messages_terminate_producer_early(self, connection):
        """Test the early return for an error in consume."""
        connection().channel().consume.return_value = self._failing_consume()
        queue = messagequeue.MessageQueue(host='host', port=123,
                                          user='user', password='password')
        callback = mock.Mock(side_effect=lambda a, b: time.sleep(1))
        queue.consume_messages('exchange', 'routing', callback)
        self.assertEqual(len(callback.mock_calls), 1)

    @staticmethod
    def _basic_consume():
        for i in range(3):
            yield (mock.Mock(routing_key='a', delivery_tag=str(i)),
                   pika.BasicProperties(), json.dumps({'c': 'd'}))
        yield (None, None, None)

    def _manual_side_effect(self, ack_fns, ack_fn, counters, cb_type):
        ack_fns.append(ack_fn)
        for _ in range(counters.pop(0)):
            if cb_type == 'ack':
                ack_fns.pop(0)(True)
            elif cb_type == 'nack':
                ack_fns.pop(0)(False)
            elif cb_type == 'missing':
                ack_fns.pop(0)()
            else:
                self.fail('Invalid cb_type')

    def _manual_ack(self, connection, expected_count, counts, cb_type='missing'):
        connection().channel().consume.return_value = self._basic_consume()
        queue = messagequeue.MessageQueue(host='host', port=123,
                                          user='user', password='password')
        ack_fns = []
        callback = mock.Mock(side_effect=lambda _, __, ack_fn:
                             self._manual_side_effect(ack_fns, ack_fn, counts, cb_type))
        queue.consume_messages('exchange', 'routing',
                               callback, manual_ack=True,
                               return_on_timeout=False)
        for callback_call in (connection().channel().connection
                              .add_callback_threadsafe.mock_calls):
            callback_call[1][0]()
        if cb_type in ('ack', 'missing'):
            self.assertEqual(len(connection().channel().basic_ack.mock_calls),
                             expected_count)
            self.assertEqual(len(connection().channel().basic_nack.mock_calls), 0)
        elif cb_type == 'nack':
            self.assertEqual(len(connection().channel().basic_nack.mock_calls),
                             expected_count)
            self.assertEqual(len(connection().channel().basic_ack.mock_calls), 0)
        else:
            self.fail('Invalid cb_type')

    @mock.patch('pika.BlockingConnection')
    def test_consume_messages_manual_ack(self, connection):
        """Test the manual acks."""
        self._manual_ack(connection, 3, [1, 1, 1, 0])

    @mock.patch('pika.BlockingConnection')
    def test_consume_messages_manual_ack_none(self, connection):
        """Test the manual acks."""
        self._manual_ack(connection, 0, [0, 0, 0, 0])

    @mock.patch('pika.BlockingConnection')
    def test_consume_messages_manual_ack_final(self, connection):
        """Test the manual acks."""
        self._manual_ack(connection, 3, [0, 0, 0, 3])

    @mock.patch('pika.BlockingConnection')
    def test_consume_messages_manual_explicit_ack(self, connection):
        """Test the manual acks."""
        self._manual_ack(connection, 3, [1, 1, 1, 0], cb_type='ack')

    @mock.patch('pika.BlockingConnection')
    def test_consume_messages_manual_explicit_nack(self, connection):
        """Test the manual nacks."""
        self._manual_ack(connection, 3, [1, 1, 1, 0], cb_type='nack')

    @mock.patch('cki_lib.messagequeue.logger.logging_env')
    @mock.patch('pika.BlockingConnection')
    def test_consume_logging_env(self, connection, logging_env):
        """Test consume_messages sets the logging environment."""
        self._test_consume(connection)

        logging_env.assert_has_calls([
            mock.call({'message': {'headers': {}}}),
            mock.call().__enter__(),
            mock.call().__exit__(mock.ANY, mock.ANY, mock.ANY,),
            mock.call({'message': {'headers': {}}}),
            mock.call().__enter__(),
            mock.call().__exit__(mock.ANY, mock.ANY, mock.ANY,),
        ])

    def _test_consume(self, connection, prefetch_count=None,
                      queue_name=None, is_production=False,
                      inactivity_timeout=None,
                      return_on_timeout=True,
                      callback_kwargs=False):
        # pylint: disable=too-many-arguments
        exchange = 'exchange'
        routing_keys = ['routing1', 'routing2']
        values = ['value1', 'value2']

        # Patch consume to return mocked values
        connection().channel().consume.return_value = [
            (mock.Mock(routing_key=f'r{v}', delivery_tag=f't{v}'),
             pika.BasicProperties(), json.dumps({'b': v})) for v in values]

        if inactivity_timeout:
            # If inactivity_timeout is set, also yield a (None, None, None) message
            connection().channel().consume.return_value += [(None, None, None)]
            if return_on_timeout:
                # If return_on_timeout, add an invalid message to stop yielding
                connection().channel().consume.return_value += [('invalid',)]

        # Make callback call fail
        callback = mock.Mock(side_effect=[Exception('boom'), mock.DEFAULT])

        # Call the messages consuming to trigger everything before checking
        queue = messagequeue.MessageQueue(host='host', port=123,
                                          user='user', password='password')
        if is_production and not queue_name:
            self.assertRaises(Exception, queue.consume_messages,
                              exchange, routing_keys, callback,
                              prefetch_count=prefetch_count,
                              queue_name=queue_name,
                              inactivity_timeout=inactivity_timeout,
                              return_on_timeout=return_on_timeout,
                              callback_kwargs=callback_kwargs)
            return

        with self.assertLogs(messagequeue.LOGGER, 'INFO') as log:
            queue.consume_messages(exchange, routing_keys, callback,
                                   prefetch_count=prefetch_count,
                                   queue_name=queue_name,
                                   inactivity_timeout=inactivity_timeout,
                                   return_on_timeout=return_on_timeout,
                                   callback_kwargs=callback_kwargs)

        # Check consume_messages logging
        self.assertTrue(any(m.startswith('INFO') for m in log.output))

        # Check connection call has connection_params
        connection.assert_called_with(queue.connection_params)

        if prefetch_count:
            # Check prefetch_count is correctly set
            connection().channel().basic_qos.assert_called_with(
                prefetch_count=prefetch_count)

        # Check queue_declare calls
        if is_production:
            connection().channel().queue_declare.assert_has_calls([
                # Main queue creation
                mock.call('queue', durable=True, arguments={
                    'x-dead-letter-exchange': 'cki.exchange.retry.incoming',
                    'x-dead-letter-routing-key': 'queue'
                }),
                # DLX retry queue creation
                mock.call('cki.queue.retry.queue', durable=True, arguments={
                    'x-dead-letter-exchange': 'cki.exchange.retry.outgoing',
                })
            ])
        else:
            # Not production, the id is a random uuid
            connection().channel().queue_declare.assert_called_with(
                mock.ANY, auto_delete=True)
            declare_calls = connection().channel().queue_declare.mock_calls
            self.assertRegex(declare_calls[0][1][0],
                             r'[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}')

        # Check queue_bind calls
        expected_bind_calls = [
            mock.call(mock.ANY, exchange, routing_key=r)
            for r in routing_keys
        ]
        if is_production:
            # On production, retry exchanges must be bound
            expected_bind_calls.extend([
                mock.call(exchange='cki.exchange.retry.incoming',
                          routing_key=queue_name,
                          queue=f'cki.queue.retry.{queue_name}'),
                mock.call(exchange='cki.exchange.retry.outgoing',
                          routing_key=queue_name,
                          queue=queue_name)
            ])
        connection().channel().queue_bind.assert_has_calls(expected_bind_calls)
        if not is_production:
            bind_calls = connection().channel().queue_bind.mock_calls
            self.assertRegex(bind_calls[0][1][0],
                             r'[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}')

        if inactivity_timeout:
            # Check consume was called with inactivity_timeout
            connection().channel().consume.assert_called_with(
                mock.ANY, inactivity_timeout=inactivity_timeout)

        # Check for callback calls
        callback_calls = (connection().channel().connection
                          .add_callback_threadsafe.mock_calls)
        if is_production:
            self.assertEqual(len(callback_calls), 2)
        else:
            # no nack send in non-prod as there are no retry queues setup
            self.assertEqual(len(callback_calls), 1)

        if is_production:
            # First message callback fails, should call nack
            callback_calls[0][1][0]()
            self.assertEqual(connection().channel().basic_nack.mock_calls,
                             [mock.call(f't{values[0]}', requeue=False)])
            # Second message callback succeeds, should call ack
            callback_calls[1][1][0]()
            self.assertEqual(connection().channel().basic_ack.mock_calls,
                             [mock.call(f't{values[1]}')])
        else:
            # only one callback, should call ack
            callback_calls[0][1][0]()
            self.assertEqual(connection().channel().basic_ack.mock_calls,
                             [mock.call(f't{values[1]}')])
            self.assertEqual(connection().channel().basic_nack.mock_calls, [])

        if callback_kwargs:
            callback_calls = [mock.call(body={'b': v},
                                        routing_key=f'r{v}',
                                        headers=None,
                                        ack_fn=None,
                                        timeout=False)
                              for v in values]
            if not return_on_timeout:
                callback_calls.append(mock.call(body=None,
                                                routing_key=None,
                                                headers=None,
                                                ack_fn=None, timeout=True))
        else:
            callback_calls = [mock.call(f'r{v}', {'b': v}) for v in values]
            if not return_on_timeout:
                callback_calls.append(
                    mock.call(None, None)
                )

        self.assertEqual(callback.mock_calls, callback_calls)

    def test_get_routing_key(self):
        """Test _get_routing_key method returns the correct routing_key."""
        properties = mock.Mock()

        test_cases = [
            # routing_key, headers, result
            ('ble', None, 'ble'),
            ('ble', {'foo': 'bar'}, 'ble'),
            ('ble', {'x-death': [{'routing-keys': ['#']}]}, '#'),
            ('ble', {'x-death': [{'routing-keys': ['not-this']}, {'routing-keys': ['#']}]}, '#'),
        ]

        for routing_key, headers, result in test_cases:
            setattr(properties, 'headers', headers)
            self.assertEqual(
                result,
                messagequeue.MessageQueue._get_routing_key(routing_key, properties)
            )

    @mock.patch('cki_lib.messagequeue.misc.is_production', mock.Mock(return_value=True))
    def test_consume_one_error_on_timeout_callback_manual_ack(self):
        # pylint: disable=protected-access
        """Test that failures on callback for timeout is correctly logged."""
        queue = messagequeue.MessageQueue(host='host', port=123,
                                          user='user', password='password')

        item, callback_kwargs, manual_ack = ('', None, '', '{}', '', ''), True, True
        with self.assertLogs(messagequeue.LOGGER, 'ERROR') as log:
            queue._consume_one(item, Exception, callback_kwargs, manual_ack)

        err_msg = (
            'Failure while processing timeout callback, '
            'messages might be left unacked and block further delivery.'
        )
        self.assertTrue(
            any(err_msg in log for log in log.output)
        )

    @mock.patch('cki_lib.messagequeue.misc.is_production', mock.Mock(return_value=True))
    def test_consume_one_error_on_timeout_callback(self):
        # pylint: disable=protected-access
        """Test that failures on callback for timeout is correctly logged."""
        queue = messagequeue.MessageQueue(host='host', port=123,
                                          user='user', password='password')

        item, callback_kwargs, manual_ack = ('', None, '', '{}', '', ''), True, False
        with self.assertLogs(messagequeue.LOGGER, 'ERROR') as log:
            queue._consume_one(item, Exception, callback_kwargs, manual_ack)

        err_msg = 'Failure while processing timeout callback'
        self.assertTrue(
            any(err_msg in log for log in log.output)
        )


class TestMessageQueueNoDLX(TestMessageQueue):
    # pylint: disable=too-many-public-methods
    """Test cki_lib/messagequeue.py with DLX disabled."""

    def _test_consume(self, connection, prefetch_count=None,
                      queue_name=None, is_production=False,
                      inactivity_timeout=None,
                      return_on_timeout=True,
                      callback_kwargs=False):
        # pylint: disable=too-many-arguments
        """Override TestMessageQueueNoDLX._test_consume to check dlx_retry=False."""
        exchange = 'exchange'
        routing_keys = ['routing1', 'routing2']
        values = ['value1', 'value2']

        # Patch consume to return mocked values
        connection().channel().consume.return_value = [
            (mock.Mock(routing_key=f'r{v}', delivery_tag=f't{v}'),
             pika.BasicProperties(), json.dumps({'b': v})) for v in values]

        if inactivity_timeout:
            # If inactivity_timeout is set, also yield a (None, None, None) message
            connection().channel().consume.return_value += [(None, None, None)]
            if return_on_timeout:
                # If return_on_timeout, add an invalid message to stop yielding
                connection().channel().consume.return_value += [('invalid',)]

        # Make callback call fail
        callback = mock.Mock(side_effect=[Exception('boom'), mock.DEFAULT])

        # Call the messages consuming to trigger everything before checking
        queue = messagequeue.MessageQueue(host='host', port=123,
                                          user='user', password='password', dlx_retry=False)
        if is_production and not queue_name:
            self.assertRaises(Exception, queue.consume_messages,
                              exchange, routing_keys, callback,
                              prefetch_count=prefetch_count,
                              queue_name=queue_name,
                              inactivity_timeout=inactivity_timeout,
                              return_on_timeout=return_on_timeout,
                              callback_kwargs=callback_kwargs)
            return

        queue.consume_messages(exchange, routing_keys, callback,
                               prefetch_count=prefetch_count,
                               queue_name=queue_name,
                               inactivity_timeout=inactivity_timeout,
                               return_on_timeout=return_on_timeout,
                               callback_kwargs=callback_kwargs)

        # Check queue_declare calls
        if is_production and queue_name:
            connection().channel().queue_declare.assert_has_calls([
                # Main queue creation, no DLX queues
                mock.call('queue', durable=True)
            ])
        else:
            # Not production, the id is a random uuid
            connection().channel().queue_declare.assert_called_with(
                mock.ANY, auto_delete=True)

        # Check queue_bind calls
        connection().channel().queue_bind.assert_has_calls([
            mock.call(mock.ANY, exchange, routing_key=r)
            for r in routing_keys
        ])

        # Check for callback calls
        callback_calls = (connection().channel().connection
                          .add_callback_threadsafe.mock_calls)
        self.assertEqual(len(callback_calls), 1)

        # Only one message callback, call ack
        callback_calls[0][1][0]()
        self.assertEqual(connection().channel().basic_ack.mock_calls,
                         [mock.call(f't{values[1]}')])

        if callback_kwargs:
            callback_calls = [mock.call(body={'b': v},
                                        routing_key=f'r{v}',
                                        headers=None,
                                        ack_fn=None,
                                        timeout=False)
                              for v in values]
            if not return_on_timeout:
                callback_calls.append(mock.call(body=None,
                                                routing_key=None,
                                                headers=None,
                                                ack_fn=None, timeout=True))
        else:
            callback_calls = [mock.call(f'r{v}', {'b': v}) for v in values]
            if not return_on_timeout:
                callback_calls.append(
                    mock.call(None, None)
                )

        self.assertEqual(callback.mock_calls, callback_calls)


class TestMessage(unittest.TestCase):
    """Test the message helpers."""

    def test_gitlab_url_job(self):
        """Test gitlab instance url from job events."""
        self.assertEqual(messagequeue.Message({
            'object_kind': 'build',
            'repository': {
                'homepage': 'https://host.name:1234/group/subgroup/project',
            }
        }).gitlab_url(), 'https://host.name:1234')

    def test_gitlab_url_pipeline(self):
        """Test gitlab instance url from pipeline events."""
        self.assertEqual(messagequeue.Message({
            'object_kind': 'pipeline',
            'project': {
                'web_url': 'https://host.name:1234/group/subgroup/project',
            },
        }).gitlab_url(), 'https://host.name:1234')

    @mock.patch('cki_lib.gitlab.get_instance')
    def test_gitlab_instance(self, get_instance):
        """Test gitlab instance can be returned."""
        get_instance.return_value = 'dummy'
        self.assertEqual(messagequeue.Message({
            'object_kind': 'pipeline',
            'project': {
                'web_url': 'https://host.name:1234/group/subgroup/project',
            },
        }).gl_instance(), 'dummy')
        get_instance.assert_called_with('https://host.name:1234')


class CallbackMock:
    """Fake callback."""

    def __init__(self, fail_on_call=None):
        """
        Initialize.

        fail_on_call: list of call number where the callback should fail.
        """
        self.messages = []
        self.fail_on_call = fail_on_call or []
        self.call_count = 0

    def __call__(self, routing_key, message):
        """
        Callback call.

        If the call count is on fail_on_call, it will raise Exception.
        Otherwise, it will store the (routing_key, message) in messages.
        """
        if self.call_count in self.fail_on_call:
            self.call_count += 1
            raise Exception()

        self.call_count += 1
        self.messages.append(
            (routing_key, message)
        )


@unittest.skipUnless(os.environ.get('RABBITMQ_HOST'), 'No rabbitmq server configured')
class IntegrationTestMessageQueue(unittest.TestCase):
    # pylint: disable=too-many-public-methods
    """Run integration tests for cki_lib/messagequeue.py."""

    def setUp(self):
        """Declare MessageQueue instance and exchanges."""
        host = os.environ['RABBITMQ_HOST']
        port = misc.get_env_int('RABBITMQ_PORT', 5672)
        management_port = misc.get_env_int('RABBITMQ_MANAGEMENT_PORT', 15672)
        user = os.environ.get('RABBITMQ_USER', 'guest')
        password = os.environ.get('RABBITMQ_PASSWORD', 'guest')

        self.messagequeue = messagequeue.MessageQueue(host=host, port=port,
                                                      user=user, password=password)

        self.queues = []
        self.exchanges = [
            'exchange.one',
            'cki.exchange.retry.incoming',
            'cki.exchange.retry.outgoing',
        ]

        with self.messagequeue.connect() as channel:
            for exchange in self.exchanges:
                channel.exchange_declare(exchange)

        self._management_if = f'http://{host}:{management_port}'
        self._management_auth = (user, password)
        SESSION.put(f'{self._management_if}/api/policies/%2F/tests-ttl',
                    auth=self._management_auth,
                    json={
                        'vhost': '/',
                        'name': 'tests-ttl',
                        'pattern': r'cki\.queue\.retry\..*',
                                   'apply-to': 'queues',
                                   'definition': {'message-ttl': 1000},
                    }).raise_for_status()

    def tearDown(self):
        """Delete all queues and exchanges from the server."""
        with self.messagequeue.connect() as channel:
            for queue in self.queues:
                with suppress(Exception):
                    channel.queue_delete(queue)
            for exchange in self.exchanges:
                with suppress(Exception):
                    channel.exchange_delete(exchange)
        SESSION.delete(f'{self._management_if}/api/policies/%2F/tests-ttl',
                       auth=self._management_auth,
                       json={
                           'vhost': '/',
                           'name': 'tests-ttl',
                           'component': 'policy',
                       }).raise_for_status()

    @mock.patch('cki_lib.messagequeue.misc.is_production', mock.Mock(return_value=True))
    def test_basic_consume(self):
        """Test publishing and receiving a message."""
        callback = CallbackMock()
        exchange = 'exchange.one'
        queue = 'queue.one'
        # Append for deletion on teardown
        self.queues.extend([
            queue,
            f'cki.queue.retry.{queue}'
        ])

        # First consume to create the queue.
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue, inactivity_timeout=0.1)
        self.assertEqual(0, callback.call_count)

        self.messagequeue.send_message({'foo': 'bar'}, '#', exchange=exchange)
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue, inactivity_timeout=0.1)

        self.assertEqual(1, callback.call_count)
        self.assertEqual(
            callback.messages,
            [('#', {'foo': 'bar'})]
        )

    @mock.patch('cki_lib.messagequeue.misc.is_production', mock.Mock(return_value=True))
    def test_basic_consume_anonymous_queue_prod_fails(self):
        """Test that anonymous queues are not allowed in production mode."""
        self.assertRaises(Exception, self.messagequeue.consume_messages,
                          'exchange.one', ['#'], CallbackMock(), inactivity_timeout=0.1)

    @mock.patch('cki_lib.messagequeue.misc.is_production', mock.Mock(return_value=True))
    def test_basic_dlx_requeue(self):
        """Test that a message is requeued after nacking it."""
        callback = CallbackMock(fail_on_call=[0])
        exchange = 'exchange.one'
        queue = 'queue.one'
        # Append for deletion on teardown
        self.queues.extend([
            queue,
            f'cki.queue.retry.{queue}'
        ])

        # First consume to create the queue.
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue, inactivity_timeout=0.01)

        # Send a message
        self.messagequeue.send_message({'foo': 'bar'}, '#', exchange=exchange)

        # First consume will raise Exception and requeue.
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue, inactivity_timeout=0.01)
        self.assertEqual(1, callback.call_count)
        self.assertEqual(0, len(callback.messages))

        # Check after a short period of time to ensure it's not requeued instantly
        time.sleep(0.5)
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue, inactivity_timeout=0.01)
        self.assertEqual(1, callback.call_count)
        self.assertEqual(0, len(callback.messages))

        # Wait until the message gets requeued
        time.sleep(1)

        # New consume will get the message again and not requeue.
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue, inactivity_timeout=0.01)
        self.assertEqual(2, callback.call_count)
        self.assertEqual(1, len(callback.messages))

        # Wait to check that the message does not get requeued
        time.sleep(1.1)

        # New consume should not get new messages.
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue, inactivity_timeout=0.01)
        self.assertEqual(2, callback.call_count)

        # Test message routing_key is correctly restored.
        self.assertListEqual(
            [('#', {'foo': 'bar'})],
            callback.messages,
        )

    @mock.patch('cki_lib.messagequeue.misc.is_production', mock.Mock(return_value=True))
    def test_basic_dlx_requeue_same_consume(self):
        """
        Test that a message is requeued after nacking it.

        On a single connection, it should get the message more than once.
        """
        callback = CallbackMock(fail_on_call=[0])
        exchange = 'exchange.one'
        queue = 'queue.one'
        # Append for deletion on teardown
        self.queues.extend([
            queue,
            f'cki.queue.retry.{queue}'
        ])

        # First consume to create the queue.
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue, inactivity_timeout=0.01)

        # Send a message
        self.messagequeue.send_message({'foo': 'bar'}, '#', exchange=exchange)

        # Consume for a long time to catch the first message and the requeued one.
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue, inactivity_timeout=2)
        self.assertEqual(2, callback.call_count)
        self.assertEqual(1, len(callback.messages))

    @mock.patch('cki_lib.messagequeue.misc.is_production', mock.Mock(return_value=True))
    def test_basic_dlx_requeue_multiple_queues(self):
        """
        Test that a message is requeued after nacking it.

        Check with multiple queues that the message is only routed into the correct one.
        """
        callback = CallbackMock(fail_on_call=[0])
        exchange = 'exchange.one'
        queue_one = 'queue.one'
        queue_two = 'queue.two'
        # Append for deletion on teardown
        self.queues.extend([
            queue_one,
            queue_two,
            f'cki.queue.retry.{queue_one}'
            f'cki.queue.retry.{queue_two}'
        ])

        # First consume to create the queues
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue_one, inactivity_timeout=0.01)
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue_two, inactivity_timeout=0.01)

        # Send a message
        self.messagequeue.send_message({'foo': 'bar'}, '#', exchange=exchange)

        # Consume on queue_one fail. Requeued
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue_one, inactivity_timeout=0.01)
        self.assertEqual(1, callback.call_count)

        # Consume on queue_two ok
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue_two, inactivity_timeout=0.01)
        self.assertEqual(2, callback.call_count)

        time.sleep(1)

        # Consume on queue_one has a new message after it was requeued
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue_one, inactivity_timeout=0.01)
        self.assertEqual(3, callback.call_count)

        # Consume on queue_two has no new messages (not requeued)
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue_two, inactivity_timeout=0.01)
        self.assertEqual(3, callback.call_count)


class TestMessageLoggingEnv(unittest.TestCase):
    """Test MessageLoggingEnv."""

    def setUp(self):
        """Set up tests."""
        self.mle = messagequeue.MessageLoggingEnv()

    def test_add_hook(self):
        """Test adding a new hook."""
        msg_header = {'message-type': 'some-message'}
        msg_body = {'foo': 'bar'}

        def hook(header, body):
            """New hook."""
            self.assertEqual(header, msg_header)
            self.assertEqual(body, msg_body)
            return {'bar': 'baz'}

        self.mle.add_hook('some-message', hook)
        self.assertIn(('some-message', hook), self.mle.hooks)

        self.assertEqual(
            self.mle.render(msg_header, msg_body),
            {
                'headers': msg_header,
                'bar': 'baz'
            }
        )

    def test_render_base(self):
        """Test render function."""
        self.assertEqual(
            self.mle.render(
                {'some_header': 'foo'},
                {'some_body': 'bar'}
            ),
            {'headers': {'some_header': 'foo'}}
        )

    def test_render_gitlab(self):
        """Test render function. Gitlab message."""
        self.assertEqual(
            self.mle.render(
                {'some_header': 'foo', 'message-type': 'gitlab'},
                {
                    'object_kind': 'build',
                    'user': {'id': 1},
                    'project': {'id': 2, 'path_with_namespace': 'foo/bar'},
                    'merge_request': {'iid': 3},
                    'object_attributes': {
                        'variables': [
                            {'key': 'mr_id', 'value': 4},
                            {'key': 'mr_project_id', 'value': 5},
                        ]
                    }
                }
            ),
            {
                'headers': {'some_header': 'foo', 'message-type': 'gitlab'},
                'gitlab': {
                    'object_kind': 'build',
                    'user': {'id': 1},
                    'project': {'id': 2, 'path_with_namespace': 'foo/bar'},
                    'merge_request': {'iid': 3},
                    'pipeline': {
                        'variables': {'mr_id': 4, 'mr_project_id': 5},
                    },
                }
            }
        )

    def test_render_datawarehouse(self):
        """Test render function. DW message."""
        self.assertEqual(
            self.mle.render(
                {'some_header': 'foo', 'message-type': 'datawarehouse'},
                {
                    'status': 'needs_triage',
                    'object_type': 'checkout',
                    'id': 1,
                    'iid': 2,
                    'misc': {'foo': 'bar'},
                }
            ),
            {
                'headers': {'some_header': 'foo', 'message-type': 'datawarehouse'},
                'datawarehouse': {
                    'status': 'needs_triage',
                    'object_type': 'checkout',
                    'id': 1,
                    'iid': 2,
                    'misc': {'foo': 'bar'},
                }
            }
        )
