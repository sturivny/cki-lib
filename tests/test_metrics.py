import time
import unittest
from unittest import mock

from freezegun import freeze_time

from cki_lib import metrics


class LoadSummaryTest(unittest.TestCase):
    """LoadSummary tests."""

    @mock.patch('cki_lib.metrics.prometheus_client.Summary', mock.Mock())
    def setUp(self):
        """Set up."""
        self.load = metrics.LoadIndex('metric_name', 'metric_description')
        self.load.metric.observe = mock.Mock()

    @freeze_time("2010-01-02 09:00:00")
    def test_enter(self):
        """Test enter method."""
        self.load.enter()
        self.assertEqual(1262422800.0, self.load.enter_ts)

    @freeze_time("2010-01-02 09:01:00")
    def test_exit(self):
        """Test enter method."""
        self.load.exit()
        self.assertEqual(1262422860.0, self.load.last_exit_ts)

    def test_exit_load(self):
        """Test enter method."""
        self.load.last_exit_ts = 1262422800.0
        self.load.enter_ts = 1262422830.0
        with freeze_time("2010-01-02 09:01:00"):
            self.load.exit()

        self.load.metric.observe.assert_called_with(0.5)

    def test_context(self):
        """Test context manager."""
        self.load.last_exit_ts = time.monotonic()  # Fake run
        time.sleep(0.1)  # Time between runs
        with self.load.context():
            time.sleep(0.1)  # Another run

        self.assertAlmostEqual(
            0.5,
            self.load.metric.observe.mock_calls[0][1][0],
            places=2
        )

    def test_context_loop(self):
        """Test context manager called many times."""
        for _ in range(10):
            time.sleep(0.01)
            with self.load.context():
                time.sleep(0.03)

        all_calls = [call[1][0] for call in self.load.metric.observe.mock_calls]
        avg_value = sum(all_calls) / len(all_calls)

        self.assertEqual(9, len(all_calls))
        self.assertAlmostEqual(0.7, avg_value, places=1)

    def test_decorator(self):
        """Test decorator called many times."""
        @self.load.context()
        def dummy_function():
            """Dummy function."""
            time.sleep(0.01)

        for _ in range(10):
            time.sleep(0.01)
            dummy_function()

        all_calls = [call[1][0] for call in self.load.metric.observe.mock_calls]
        avg_value = sum(all_calls) / len(all_calls)

        self.assertEqual(9, len(all_calls))
        self.assertAlmostEqual(0.5, avg_value, places=1)


class PrometheusInitTest(unittest.TestCase):
    """Test prometheus_init method."""

    @mock.patch.dict('os.environ', {'CKI_METRICS_ENABLED': 'True', 'CKI_METRICS_PORT': '1234'})
    @mock.patch('cki_lib.metrics.prometheus_client')
    def test_enabled(self, prometheus_client):
        """Test when enabled."""
        metrics.prometheus_init()

        self.assertTrue(prometheus_client.start_http_server.called)
        prometheus_client.start_http_server.assert_called_with(1234)

    @mock.patch.dict('os.environ', {'CKI_METRICS_ENABLED': 'False'})
    @mock.patch('cki_lib.metrics.prometheus_client')
    def test_disabled(self, prometheus_client):
        """Test when disabled."""
        metrics.prometheus_init()

        self.assertFalse(prometheus_client.start_http_server.called)

    def test_default(self):
        """Test default value."""
        prometheus = mock.Mock()
        metrics.prometheus_init(prometheus)

        self.assertFalse(prometheus.start_http_server.called)
